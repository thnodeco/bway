<?php
/**
 * The sidebar containing the main widget area.
 *
 * @package understrap
 */

if ( ! is_active_sidebar( 'left-sidebar' ) ) {
	return;
}

// when both sidebars turned on reduce col size to 3 from 4.
$sidebar_pos = get_theme_mod( 'understrap_sidebar_position' );
?>

<?php if ( 'both' === $sidebar_pos ) : ?>
	<div class="col-md-3 widget-area" id="left-sidebar" role="complementary">
<?php else : ?>
	<div class="col-md-2 widget-area" id="left-sidebar" role="complementary">
<?php endif; ?>

<?php
if ( is_post_type_archive( 'leadership' ) || is_singular('leadership') ) {
    dynamic_sidebar( 'leadership-side-bar' );
} else {
    dynamic_sidebar( 'left-sidebar' );
}
?>

</div><!-- #secondary -->
