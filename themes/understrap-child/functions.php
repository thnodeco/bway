<?php
function understrap_remove_scripts() {
    wp_dequeue_style( 'understrap-styles' );
    wp_deregister_style( 'understrap-styles' );

    wp_dequeue_script( 'understrap-scripts' );
    wp_deregister_script( 'understrap-scripts' );

    // Removes the parent themes stylesheet and scripts from inc/enqueue.php
}
add_action( 'wp_enqueue_scripts', 'understrap_remove_scripts', 20 );

add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );
function theme_enqueue_styles() {

	// Get the theme data
	$the_theme = wp_get_theme();

    wp_enqueue_style( 'child-understrap-styles', get_stylesheet_directory_uri() . '/css/child-theme.min.css', array(), $the_theme->get( 'Version' ) );
    wp_enqueue_style( 'jqueryImageReveal-styles', get_stylesheet_directory_uri() . '/css/jquery.imageReveal.css', array(), $the_theme->get( 'Version' ) );
    wp_enqueue_style( 'flexSlider-styles', get_stylesheet_directory_uri() . '/css/flexslider.css', array(), $the_theme->get( 'Version' ) );
    wp_enqueue_style( 'font-awesome-styles', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css', array(), $the_theme->get( 'Version' ) );

    wp_enqueue_script( 'child-understrap-scripts', get_stylesheet_directory_uri() . '/js/child-theme.min.js', array(), $the_theme->get( 'Version' ), true );
    wp_enqueue_script( 'jqueryImageReveal', get_stylesheet_directory_uri() . '/js/jquery.imageReveal.js', array(), $the_theme->get( 'Version' ), true );
    wp_enqueue_script( 'flexSlider', get_stylesheet_directory_uri() . '/js/jquery.flexslider-min.js', array(), $the_theme->get( 'Version' ), true );

  // Main JS & Styles for Timeline Our Stories Page
    if ( is_page_template( 'page-templates/timeline.php' ) ) {

      wp_enqueue_style( 'animate-styles', get_stylesheet_directory_uri() . '/css/plugins/animate.min.css', array(), $the_theme->get( 'Version' ) );
      wp_enqueue_style( 'simple-line-icons-styles', get_stylesheet_directory_uri() . '/css/plugins/simple-line-icons.css', array(), $the_theme->get( 'Version' ) );
      wp_enqueue_style( 'default-styles', get_stylesheet_directory_uri() . '/css/default.min.css', array(), $the_theme->get( 'Version' ) );
      wp_enqueue_style( 'first-timeline-18-styles', get_stylesheet_directory_uri() . '/css/frst-timeline-style-18.css', array(), $the_theme->get( 'Version' ) );

      wp_enqueue_script( 'first-scale-effect', get_stylesheet_directory_uri() . '/js/frst-scale-effect.js', array(), $the_theme->get( 'Version' ), true );
      wp_enqueue_script( 'first-timeline', get_stylesheet_directory_uri() . '/js/frst-timeline.min.js', array(), $the_theme->get( 'Version' ), true );
      wp_enqueue_script( 'modernizr', get_stylesheet_directory_uri() . '/js/modernizr.js', array(), $the_theme->get( 'Version' ), true );

    }
  // End Our Stories

  // Change number or products per row to 5
  add_filter('loop_shop_columns', 'loop_columns');
  if (!function_exists('loop_columns')) {
  	function loop_columns() {
  		return 5; // 5 products per row
  	}

  // remove default sorting dropdown
  remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );
    }

    add_filter( 'woocommerce_product_tabs', 'woo_rename_tabs', 98 );
    function woo_rename_tabs( $tabs ) {

    	$tabs['description']['title'] = __( 'Specifications' );		// Rename the description tab
//    	$tabs['reviews']['title'] = __( 'Ratings' );				// Rename the reviews tab
//    	$tabs['additional_information']['title'] = __( 'Product Data' );	// Rename the additional information tab

    	return $tabs;

    }

    // Remove the description tab heading
    add_filter( 'woocommerce_product_description_heading', 'wc_change_product_description_tab_heading', 10, 1 );
    function wc_change_product_description_tab_heading( $title ) {
    	global $post;
    	return $tabs;
    }

    add_filter( 'woocommerce_product_tabs', 'woo_remove_product_tabs', 98 );

    function woo_remove_product_tabs( $tabs ) {

        unset( $tabs['description'] );      	// Remove the description tab
        unset( $tabs['reviews'] ); 			// Remove the reviews tab
        unset( $tabs['additional_information'] );  	// Remove the additional information tab

        return $tabs;

    }

    /** Remove Related Products from Single Product Template */
    remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );

    /** Remove short description if product tabs are not displayed */
    function dot_reorder_product_page() {
        if ( get_option('woocommerce_product_tabs') == false ) {
            remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20 );
        }
    }
    add_action( 'woocommerce_before_main_content', 'dot_reorder_product_page' );

    /** Display product description the_content */
    function dot_do_product_desc() {

        global $woocommerce, $post;

        if ( $post->post_content ) : ?>
            <div itemprop="description" class="item-description">
                <?php $heading = apply_filters('woocommerce_product_description_heading', __('Product Description', 'woocommerce')); ?>

                <!-- <h2><?php echo $heading; ?></h2> -->
                <?php the_content(); ?>

            </div>
        <?php endif;
    }
    add_action( 'woocommerce_single_product_summary', 'dot_do_product_desc', 20 );

}

// Leadership Members Section
if ( ! function_exists('leadership') ) {

// Register Leadership Post Type
function leadership() {

	$labels = array(
		'name'                  => _x( 'Leadership Members', 'Post Type General Name', 'bway' ),
		'singular_name'         => _x( 'Leadership', 'Post Type Singular Name', 'bway' ),
		'menu_name'             => __( 'Leadership', 'bway' ),
		'name_admin_bar'        => __( 'Leadership', 'bway' ),
		'archives'              => __( 'Leadership Archives', 'bway' ),
		'attributes'            => __( 'Leadership Attributes', 'bway' ),
		'parent_item_colon'     => __( 'Parent Item:', 'bway' ),
		'all_items'             => __( 'All Leaders', 'bway' ),
		'add_new_item'          => __( 'Add New Member', 'bway' ),
		'add_new'               => __( 'Add New', 'bway' ),
		'new_item'              => __( 'New Member', 'bway' ),
		'edit_item'             => __( 'Edit Member', 'bway' ),
		'update_item'           => __( 'Update Member', 'bway' ),
		'view_item'             => __( 'View Member', 'bway' ),
		'view_items'            => __( 'View Members', 'bway' ),
		'search_items'          => __( 'Search Member', 'bway' ),
		'not_found'             => __( 'Not found', 'bway' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'bway' ),
		'featured_image'        => __( 'Leader Headshot', 'bway' ),
		'set_featured_image'    => __( 'Set Leader Headshot', 'bway' ),
		'remove_featured_image' => __( 'Remove Leader Headshot', 'bway' ),
		'use_featured_image'    => __( 'Use as Leader Headshot', 'bway' ),
		'insert_into_item'      => __( 'Insert into item', 'bway' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'bway' ),
		'items_list'            => __( 'Items list', 'bway' ),
		'items_list_navigation' => __( 'Items list navigation', 'bway' ),
		'filter_items_list'     => __( 'Filter items list', 'bway' ),
	);
	$args = array(
		'label'                 => __( 'Leadership', 'bway' ),
		'description'           => __( 'Leadership Members', 'bway' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'author', 'thumbnail', 'revisions', ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-groups',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'leadership', $args );

}
add_action( 'init', 'leadership', 0 );

}

//* Add Role Taxonomy for Leadership Post Type
add_action( 'init', 'leader_role_type_taxonomy', 0 );
function leader_role_type_taxonomy() {
  register_taxonomy( 'leader_role', array( 'leadership' ), array(
      'labels' => array(
        'name' => _x( 'Role Type', 'taxonomy general name' ),
        'singular_name' => _x( 'Role Type', 'taxonomy singular name' ),
        'search_items' => __( 'Search Role Types' ),
        'all_items' => __( 'All Role Types' ),
        'parent_item' => __( 'Parent Role Type' ),
        'parent_item_colon' => __( 'Parent Role Type:' ),
        'update_item' => __( 'Update Role Type' ),
        'edit_item' => __( 'Edit Role Type' ),
        'add_new_item' => __( 'Add New Role Type' ),
        'new_item_name' => __( 'New Role Type' ),
        'menu_name' => __( 'Role Types' ),
      ),
      'rewrite' => array(
        'slug' => 'role-type'
      ),
      'hierarchical' => true,
    )
  );
}

// Custom Sidebar for Leadership Page - Allows separate logic for left sidebar on only the leadership page, and allows page specific widgets under the Widget admin
function leadership_sidebar() {
    register_sidebar(
        array (
            'name' => __( 'Leadership', 'bway' ),
            'id' => 'leadership-side-bar',
            'description' => __( 'Leadership Sidebar', 'bway' ),
            'before_widget' => '<div class="widget-content">',
            'after_widget' => "</div>",
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        )
    );
}
add_action( 'widgets_init', 'leadership_sidebar' );

// Custom Blog Layout Functionality extracted and modifed from inc/template-tags.php
if ( ! function_exists( 'understrap_posted_on' ) ) :
/**
 * Prints HTML with meta information for the current post-date/time and author.
 */
function understrap_posted_on() {
	$time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time>';

	$time_string = sprintf( $time_string,
		esc_attr( get_the_date( 'c' ) ),
		esc_html( get_the_date() )
	);
	$posted_on = sprintf(
		esc_html_x( 'Posted on %s', 'post date', 'understrap' ),
		'<a href="' . esc_url( get_permalink() ) . '" rel="bookmark">' . $time_string . '</a>'
	);
	$byline = sprintf(
		esc_html_x( 'Post by %s ', 'post author', 'understrap' ),
		'<span class="author vcard"><a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '">' . esc_html( get_the_author() ) . '</a></span>'
	);
	echo '
  <i class="fa fa-user-o" aria-hidden="true"></i><span class="byline mr-2"> ' . $byline . '</span><i class="fa fa-calendar" aria-hidden="true"></i> <span class="posted-on mr-2">' . $posted_on . '</span>'; // WPCS: XSS OK.
}
endif;

if ( ! function_exists( 'understrap_entry_footer' ) ) :
/**
 * Prints HTML with meta information for the categories, tags and comments.
 */
function understrap_entry_footer() {
	// Hide category and tag text for pages.
	if ( 'post' === get_post_type() ) {
		/* translators: used between list items, there is a space after the comma */
		$categories_list = get_the_category_list( esc_html__( ', ', 'understrap' ) );
		if ( $categories_list && understrap_categorized_blog() ) {
			printf( '<span class="cat-links">' . esc_html__( 'Posted in %1$s', 'understrap' ) . '</span>', $categories_list ); // WPCS: XSS OK.
		}
		/* translators: used between list items, there is a space after the comma */
		$tags_list = get_the_tag_list( '', esc_html__( ', ', 'understrap' ) );
		if ( $tags_list ) {
			printf( '<span class="tags-links">' . esc_html__( 'Tagged %1$s', 'understrap' ) . '</span>', $tags_list ); // WPCS: XSS OK.
		}
	}
	if ( ! is_single() && ! post_password_required() && ( comments_open() || get_comments_number() ) ) {
		echo ' <i class="fa fa-comments-o" aria-hidden="true"></i><span class="comments-link ml-1">';
		comments_popup_link( esc_html__( 'Leave a comment', 'understrap' ), esc_html__( '1 Comment', 'understrap' ), esc_html__( '% Comments', 'understrap' ) );
		echo '</span>';
	}
	edit_post_link(
		sprintf(
			/* translators: %s: Name of current post */
			esc_html__( 'Edit %s', 'understrap' ),
			the_title( '<span class="screen-reader-text">"', '"</span>', false )
		),
		'<span class="edit-link ml-5">',
		'</span>'
	);
}
endif;

/**
 * Filter the except length to 20 words.
 *
 * @param int $length Excerpt length.
 * @return int (Maybe) modified excerpt length.
 */
function custom_excerpt_length( $length ) {
    return 42;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

if ( ! function_exists( 'understrap_widgets_init' ) ) {
	/**
	 * * Initializes themes widgets (These have been customized and now override the default found in inc/widgets.php in parent theme).
	 */
	function understrap_widgets_init() {
		register_sidebar( array(
			'name'          => __( 'Right Sidebar', 'understrap' ),
			'id'            => 'right-sidebar',
			'description'   => 'Right sidebar widget area',
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h3 class="widget-title">',
			'after_title'   => '</h3>',
		) );

		register_sidebar( array(
			'name'          => __( 'Left Sidebar', 'understrap' ),
			'id'            => 'left-sidebar',
			'description'   => 'Left sidebar widget area',
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h3 class="widget-title cat-border">',
			'after_title'   => '</h3>',
		) );

		register_sidebar( array(
			'name'          => __( 'Hero Slider', 'understrap' ),
			'id'            => 'hero',
			'description'   => 'Hero slider area. Place two or more widgets here and they will slide!',
			'before_widget' => '<div class="carousel-item">',
			'after_widget'  => '</div>',
			'before_title'  => '',
			'after_title'   => '',
		) );

		register_sidebar( array(
			'name'          => __( 'Hero Static', 'understrap' ),
			'id'            => 'statichero',
			'description'   => 'Static Hero widget. no slider functionallity',
		    'before_widget'  => '<div id="%1$s" class="static-hero-widget %2$s '. slbd_count_widgets( 'statichero' ) .'">',
		    'after_widget'   => '</div><!-- .static-hero-widget -->',
		    'before_title'   => '<h3 class="widget-title">',
		    'after_title'    => '</h3>',
		) );

		register_sidebar( array(
			'name'          => __( 'Footer Full', 'understrap' ),
			'id'            => 'footerfull',
			'description'   => 'Widget area below main content and above footer',
		    'before_widget'  => '<div id="%1$s" class="footer-widget %2$s '. slbd_count_widgets( 'footerfull' ) .'">',
		    'after_widget'   => '</div><!-- .footer-widget -->',
		    'before_title'   => '<h3 class="widget-title">',
		    'after_title'    => '</h3>',
		) );

	}
} // endif function_exists( 'understrap_widgets_init' ).
