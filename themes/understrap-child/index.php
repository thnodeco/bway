<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package understrap
 */

get_header();

$container   = get_theme_mod( 'understrap_container_type' );
$sidebar_pos = 'right';
?>

<div class="container-fluid">
	<div class="row justify-content-end newsheader" style="background: url('<?php echo site_url(); ?>/wp-content/uploads/2017/09/BWAY-142_News_Events_4000x550_3.jpg') no-repeat center center; position: relative; background-size: cover; min-height: 550px;">
		<div class="col-12 col-md-3 mr-md-5 float-right"><h1 style="color: #fff; font-size: 2.2rem;margin-top: 50%;">NEWS AND EVENTS</h1></div>
	</div>
</div>

<div class="wrapper" id="wrapper-index">

	<div class="container-fluid ml-md-5 mr-md-5 pl-md-5 pr-md-5 mt-md-5" id="content" tabindex="-1">

		<div class="row mb-5">

			<!-- Do the left sidebar check and opens the primary div -->
			<div class="col-md-9 content-area" id="primary">

			<main class="site-main" id="main">

				<?php if ( have_posts() ) : ?>

					<?php /* Start the Loop */ ?>

					<?php while ( have_posts() ) : the_post(); ?>

						<?php

						/*
						 * Include the Post-Format-specific template for the content.
						 * If you want to override this in a child theme, then include a file
						 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
						 */
						get_template_part( 'loop-templates/content', get_post_format() );
						?>

					<?php endwhile; ?>

				<?php else : ?>

					<?php get_template_part( 'loop-templates/content', 'none' ); ?>

				<?php endif; ?>

			</main><!-- #main -->

			<!-- The pagination component -->
			<?php understrap_pagination(); ?>

		</div><!-- #primary -->

		<!-- Do the right sidebar check -->

			<?php get_sidebar( 'right' ); ?>

	</div><!-- .row -->

</div><!-- Container end -->

</div><!-- Wrapper end -->

<?php get_footer(); ?>
