<?php
/**
 * The template for displaying archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package understrap
 */

get_header();
?>

<?php
$container   = get_theme_mod( 'understrap_container_type' );
$sidebar_pos = get_theme_mod( 'understrap_sidebar_position' );
?>

<div class="container-fluid">
	<div class="row justify-content-center" style="background: url('<?php echo site_url(); ?>/wp-content/uploads/2017/08/BWAY-123_Leadership_4000x550.jpg') no-repeat center center; position: relative; background-size: cover; min-height: 450px;">
	</div>
</div>

			<header class="page-header">
				<div class="container">
					<div class="row justify-content-center" style="margin: 40px 0">
						<h1 class="entry-title" style="color: #004b8d; font-weight: bold; text-transform: uppercase;">Leadership</h1>
					</div>
				</div>
			</header><!-- .page-header -->

			<div class="container">
				<div class="row mb-3">
					<p class="text-center">BWAY’s management team is comprised of visionary, experienced and passionate leaders whose diverse perspectives and stewardship remain the driving force behind our success. Under their direction, BWAY is shaping the future of the packaging industry and creating long-lasting value for our customers. </p>
				</div>
			</div>

<div class="wrapper" id="archive-wrapper">

	<div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">

		<div class="row">

			<!-- Do the left sidebar check -->
			<?php get_template_part( 'global-templates/left-sidebar-check', 'none' ); ?>

			<main class="site-main" id="main">

				<div class="container-fluid">
					<div class="row">
						<h3 class="title mb-4">Stone Canyon Industries Packaging</h3>
					</div>
				</div>
				<div class="container-fluid">
					<div class="row">
							<?php //* The Query
							$exec_query = new WP_Query( array (
								'post_type' => 'leadership',
								'leader_role'  => 'stone-canyon',
								'posts_per_page' => 5
							) );

							//* The Loop
							if ( $exec_query->have_posts() ) { ?>

										<?php while ( $exec_query->have_posts() ): $exec_query->the_post(); ?>

											<div class="col-md-2 ml-1 mr-1 mb-5">

													<center><a href="<?php the_permalink(); ?>"><img src="<?php echo get_the_post_thumbnail_url( $post->ID, 'full' ); ?>" alt="<?php echo $name ?>" style="border: 2px solid #ececec; margin-bottom: 10px;"/></a><br />
													<a href="<?php the_permalink(); ?>" style="font-weight:bold;"><?php the_title(); ?></a><br />
													<span style="font-size: 12px; width: 164px; color: #797979; line-height: 1.3; display: block"><?php the_field('title'); ?></span></center>

											</div>

									<?php endwhile; ?>
					</div>
				</div>

					<?php //* Restore original Post Data
						wp_reset_postdata();} ?>

					<div class="container-fluid">
						<div class="row">
							<h3 class="title mb-4">BWAY Executive Leadership</h3>
						</div>
					</div>
					<div class="container-fluid">
						<div class="row">
								<?php //* The Query
								$exec_query = new WP_Query( array (
								  'post_type' => 'leadership',
								  'leader_role'  => 'executives',
								  'posts_per_page' => 5
								) );

								//* The Loop
								if ( $exec_query->have_posts() ) { ?>

											<?php while ( $exec_query->have_posts() ): $exec_query->the_post(); ?>

												<div class="col-md-2 ml-1 mr-1 mb-5">

														<center><a href="<?php the_permalink(); ?>"><img src="<?php echo get_the_post_thumbnail_url( $post->ID, 'full' ); ?>" alt="<?php echo $name ?>" style="border: 2px solid #ececec; margin-bottom: 10px;"/></a><br />
														<a href="<?php the_permalink(); ?>" style="font-weight:bold;"><?php the_title(); ?></a><br />
														<span style="font-size: 12px; width: 164px; color: #797979; line-height: 1.3; display: block"><?php the_field('title'); ?></span></center>

												</div>

										<?php endwhile; ?>
						</div>
					</div>

						<?php //* Restore original Post Data
					    wp_reset_postdata();} ?>

							<div class="container-fluid">
								<div class="row">
									<h3 class="title mb-4">BWAY Corporate Vice Presidents</h3>
								</div>
							</div>
							<div class="container-fluid">
								<div class="row">

							<?php //* The Query
							$corp_query = new WP_Query( array (
							  'post_type' => 'leadership',
							  'leader_role'  => 'corporate-vice-presidents',
							  'posts_per_page' => 20
							) );

							//* The Loop
							if ( $corp_query->have_posts() ) { ?>

										<?php while ( $corp_query->have_posts() ): $corp_query->the_post(); ?>

											<div class="col-md-2 ml-1 mr-1 mb-5">

													<center><a href="<?php the_permalink(); ?>"><img src="<?php echo get_the_post_thumbnail_url( $post->ID, 'full' ); ?>" alt="<?php echo $name ?>" style="border: 2px solid #ececec; margin-bottom: 10px;"/></a><br />
													<a href="<?php the_permalink(); ?>" style="font-weight:bold;"><?php the_title(); ?></a><br />
													<span style="font-size: 12px; width: 164px; color: #797979; line-height: 1.3; display: block"><?php the_field('title'); ?></span></center>

											</div>

									<?php endwhile; ?>
								</div>
							</div>
								<?php //* Restore original Post Data
							    wp_reset_postdata();} ?>

				</div>
			</div>

			</main><!-- #main -->

		</div><!-- #primary -->

		<!-- Do the right sidebar check -->
		<?php if ( 'right' === $sidebar_pos || 'both' === $sidebar_pos ) : ?>

			<?php get_sidebar( 'right' ); ?>

		<?php endif; ?>

	</div> <!-- .row -->

</div><!-- Container end -->

</div><!-- Wrapper end -->

<?php get_footer(); ?>
