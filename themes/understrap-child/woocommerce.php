<?php
/**
 * The template for displaying all woocommerce pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package understrap
 */

get_header();

$container   = get_theme_mod( 'understrap_container_type' );
$sidebar_pos = get_theme_mod( 'understrap_sidebar_position' );
$current_term = get_queried_object();
$image = get_field('category_image', $current_term );
$upload_dir = wp_upload_dir();
?>


<?php
if ( is_product() ) { ?>
 <?php } else { ?>
	<div class="container-fluid">
		<div class="row justify-content-center pg-headers" style="background: url('<?php echo $image ?>') no-repeat center center;">
		</div>
	</div>
<?php } ?>

<div class="wrapper" id="woocommerce-wrapper">

	<div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">

		<div class="row">

			<!-- Do the left sidebar check -->
			<?php get_template_part( 'global-templates/left-sidebar-check', 'none' ); ?>

			<main class="site-main clearfix" id="main">

			<?php
				$template_name = '\archive-product.php';
				$args = array();
				$template_path = '';
				$default_path = untrailingslashit( plugin_dir_path(__FILE__) ) . '\woocommerce';

					if ( is_singular( 'product' ) ) {

						woocommerce_content();

			//For ANY product archive, Product taxonomy, product search or /shop landing page etc Fetch the template override;
				} 	elseif ( file_exists( $default_path . $template_name ) )
					{
					wc_get_template( $template_name, $args, $template_path, $default_path );

			//If no archive-product.php template exists, default to catchall;
				}	else  {
					woocommerce_content( );
				}

			;?>

			</main><!-- #main -->

		<!-- #primary -->

    <div class="container-fluid">
      <div class="row justify-content-center" style="margin: 100px 0;">
				<div class="col-sm-12 col-md-8 col-lg-6">
          <h4 style="color: #58595b; text-align: center; font-family: DIN-Medium; font-weight: bold; text-transform:uppercase">Have Questions<br />About Our Products?</h4>
          <div class="container">
            <div class="row justify-content-center mt-5">
              <div class="col-sm-3 col-md-3">
                <center><a href="tel:8005272267" title="Call Us at 800.527.2267"><img src="<?php echo $upload_dir['baseurl']; ?>/2017/07/phone-icon.png" alt="Call Us" /></a></center><br />
                <p style="text-align: center; font-weight: bold;"><a href="tel:8005272267" title="Call Us at 800.527.2267">800.527.2267</a></p>
              </div>
              <div class="col-sm-3 col-md-3">
                <center><a href="mailto:sales@bwaycorp.com?subject=Product%20Question" title="Contact Us"><img src="<?php echo $upload_dir['baseurl']; ?>/2017/07/email-icon.png" alt="Email Us" /></a></center><br />
                <p style="text-align: center; font-weight: bold;"><a href="mailto:sales@bwaycorp.com?subject=Product%20Question" title="Contact Us">EMAIL</a></p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

		<!-- Do the right sidebar check -->
		<?php if ( 'right' === $sidebar_pos || 'both' === $sidebar_pos ) : ?>

			<?php get_sidebar( 'right' ); ?>

		<?php endif; ?>

	</div><!-- .row -->



</div><!-- Container end -->

</div><!-- Wrapper end -->

<?php get_footer(); ?>
