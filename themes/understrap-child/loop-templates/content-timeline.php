<?php
/**
 * Partial template for content in full-contained.php
 *
 * @package understrap
 */

?>
<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">

	<div class="entry-content">

		<div class="container">
			<div class="row justify-content-center" style="margin: 40px 0 40px 0;">
				<?php the_title( '<h1 class="entry-title" style="color: #004b8d; font-weight: bold; text-transform: uppercase;">', '</h1>' ); ?>
			</div>
		</div>

		<div class="container">
			<div class="row mb-4">
				<?php the_content(); ?>
			</div>
		</div>

		<div class="container">
			<div id="page">
				<div class="frst-container" data-animation-name="slideInUp">
						<!-- frst-left-align/frst-right-align/frst-alternate/frst-date-opposite/frst-responsive-right-->
						<div class="frst-timeline frst-timeline-style-18 frst-alternate frst-date-opposite">
								<!-- first-year-label -->
								<div class="frst-timeline-block frst-timeline-label-block" data-animation="slideInUp">
										<div class="frst-labels">
												<span>1875</span>
										</div>
										<!-- .frst-start-label -->
								</div>
								<!-- right-timeline-block -->
								<div class="frst-timeline-block frst-odd-item" data-animation="slideInUp">
										<div class="frst-timeline-img">
												<span><i class="fa fa-building-o" aria-hidden="true"></i></span>
										</div>
										<!-- frst-timeline-img -->
										<div class="frst-timeline-content">
										 <div class="frst-timeline-content-inner">
														<img src="<?php echo site_url(); ?>/wp-content/themes/understrap-child/images/timeline/BWAY-115_Timeline_1_1875_Will_Vogel.jpg" />
														<div class="text-content">
														<p>In 1875, a private family business owned by William Vogel of Brooklyn, New York, starts making tin plate pots, pans, pails, water coolers, pie plates, cuspidors and other household products.</p></div>
												</div>
												<!-- frst-timeline-content-inner -->
										</div>
										<!-- frst-timeline-content -->
								</div>
								<!-- next-year-label -->
								<div class="frst-timeline-block frst-timeline-label-block" data-animation="slideInUp">
										<div class="frst-labels frst-start-label">
												<span>1929</span>
										</div>
										<!-- .frst-start-label -->
								</div>
								<!-- left-timeline-block -->
								<div class="frst-timeline-block frst-even-item" data-animation="slideInUp">
										<div class="frst-timeline-img">
												<span><i class="fa fa-bug"></i></span>
										</div>
										<!-- frst-timeline-img -->
										<div class="frst-timeline-content">
												<div class="frst-timeline-content-inner">
														<img src="<?php echo site_url(); ?>/wp-content/themes/understrap-child/images/timeline/BWAY-115_Timeline_2_1929_Sprayers.jpg" />
														<div class="text-content">
														<p>In 1929, Vogel's grandson, William Martin Vogel, re-establishes the family business as Standard Sprayer, Inc. The company manufactures hand insecticide sprayers and pie plates.</p></div>
												</div>
												<!-- frst-timeline-content-inner -->
										</div>
										<!-- frst-timeline-content -->
								</div>
								<!-- next-year-label -->
								<div class="frst-timeline-block frst-timeline-label-block" data-animation="slideInUp">
										<div class="frst-labels">
												<span>1942</span>
										</div>
										<!-- .frst-start-label -->
								</div>
								<!-- right-timeline-block -->
								<div class="frst-timeline-block frst-odd-item" data-animation="slideInUp">
										<div class="frst-timeline-img">
												<span><i class="fa fa-male" aria-hidden="true"></i></span>
										</div>
										<!-- frst-timeline-img -->
										<div class="frst-timeline-content">
										 <div class="frst-timeline-content-inner">
														<img src="<?php echo site_url(); ?>/wp-content/themes/understrap-child/images/timeline/BWAY-115_Timeline_3_1942_Ammo_Can.jpg" />
														<div class="text-content">
														<p>In 1942, Standard Container begins producing ammunition boxes, shell containers and other military supplies around-the-clock for American soldiers in World War II. After the war, the company resumes production of sprayers, pie plates and truck filters.</p></div>
												</div>
												<!-- frst-timeline-content-inner -->
										</div>
										<!-- frst-timeline-content -->
								</div>
								<!-- next-year-label -->
								<div class="frst-timeline-block frst-timeline-label-block" data-animation="slideInUp">
										<div class="frst-labels frst-start-label">
												<span>1957</span>
										</div>
										<!-- .frst-start-label -->
								</div>
								<!-- left-timeline-block -->
								<div class="frst-timeline-block frst-even-item" data-animation="slideInUp">
										<div class="frst-timeline-img">
												<span><i class="fa fa-building-o"></i></span>
										</div>
										<!-- frst-timeline-img -->
										<div class="frst-timeline-content">
												<div class="frst-timeline-content-inner">
														<img src="<?php echo site_url(); ?>/wp-content/themes/understrap-child/images/timeline/BWAY-115_Timeline_4_1957_Homerville_GA.jpg" />
														<div class="text-content">
														<p>In 1957, the company reorganizes as Standard Container Co. and builds a 30,000-square-foot plant in Homerville, Georgia. Clinch County residents invested 50,000 to bring the company to the area.</p></div>
												</div>
												<!-- frst-timeline-content-inner -->
										</div>
										<!-- frst-timeline-content -->
								</div>
								<!-- next-year-label -->
								<div class="frst-timeline-block frst-timeline-label-block" data-animation="slideInUp">
										<div class="frst-labels">
												<span>1962</span>
										</div>
										<!-- .frst-start-label -->
								</div>
								<!-- right-timeline-block -->
								<div class="frst-timeline-block frst-odd-item" data-animation="slideInUp">
										<div class="frst-timeline-img">
												<span><i class="fa fa-paint-brush" aria-hidden="true"></i></span>
										</div>
										<!-- frst-timeline-img -->
										<div class="frst-timeline-content">
										 <div class="frst-timeline-content-inner">
														<img src="<?php echo site_url(); ?>/wp-content/themes/understrap-child/images/timeline/BWAY-115_Timeline_4_1962_Standard_Containers.jpg" />
														<div class="text-content">
														<p>In 1962, Standard Container enters the gallon tin plate paint can business.</p></div>
												</div>
												<!-- frst-timeline-content-inner -->
										</div>
										<!-- frst-timeline-content -->
								</div>
								<!-- next-year-label -->
								<div class="frst-timeline-block frst-timeline-label-block" data-animation="slideInUp">
										<div class="frst-labels frst-start-label">
												<span>1968</span>
										</div>
										<!-- .frst-start-label -->
								</div>
								<!-- left-timeline-block -->
								<div class="frst-timeline-block frst-even-item" data-animation="slideInUp">
										<div class="frst-timeline-img">
												<span><i class="fa fa-usd"></i></span>
										</div>
										<!-- frst-timeline-img -->
										<div class="frst-timeline-content">
												<div class="frst-timeline-content-inner">
														<img src="<?php echo site_url(); ?>/wp-content/themes/understrap-child/images/timeline/BWAY-115_Timeline_6_1968_ASE_Logo.jpg" />
														<div class="text-content">
														<p>In 1968, Standard Container becomes a public company and is listed on the American Stock Exchange.</p></div>
												</div>
												<!-- frst-timeline-content-inner -->
										</div>
										<!-- frst-timeline-content -->
								</div>
								<!-- next-year-label -->
								<div class="frst-timeline-block frst-timeline-label-block" data-animation="slideInUp">
										<div class="frst-labels">
												<span>1969</span>
										</div>
										<!-- .frst-start-label -->
								</div>
								<!-- right-timeline-block -->
								<div class="frst-timeline-block frst-odd-item" data-animation="slideInUp">
										<div class="frst-timeline-img">
												<span><i class="fa fa-usd" aria-hidden="true"></i></span>
										</div>
										<!-- frst-timeline-img -->
										<div class="frst-timeline-content">
										 <div class="frst-timeline-content-inner">
														<img src="<?php echo site_url(); ?>/wp-content/themes/understrap-child/images/timeline/BWAY-115_Timeline_7_1969_Steel_Pails.jpg" />
														<div class="text-content">
														<p>In 1969, Standard Container enters the steel pail business after buying Zuck Pail and Can Co. The company also begins making metal egg cans after acquiring Interstate Can Co., Inc.</p></div>
												</div>
												<!-- frst-timeline-content-inner -->
										</div>
										<!-- frst-timeline-content -->
								</div>
								<!-- next-year-label -->
								<div class="frst-timeline-block frst-timeline-label-block" data-animation="slideInUp">
										<div class="frst-labels frst-start-label">
												<span>1973</span>
										</div>
										<!-- .frst-start-label -->
								</div>
								<!-- left-timeline-block -->
								<div class="frst-timeline-block frst-even-item" data-animation="slideInUp">
										<div class="frst-timeline-img">
												<span><i class="fa fa-industry"></i></span>
										</div>
										<!-- frst-timeline-img -->
										<div class="frst-timeline-content">
												<div class="frst-timeline-content-inner">
														<img src="<?php echo site_url(); ?>/wp-content/themes/understrap-child/images/timeline/BWAY-115_Timeline_8_1973_Plastic_Pails.jpg" />
														<div class="text-content">
														<p>In 1973, Standard Container begins manufacturing 5-gallon plastic pails and enters the fuel can business.</p></div>
												</div>
												<!-- frst-timeline-content-inner -->
										</div>
										<!-- frst-timeline-content -->
								</div>
								<!-- next-year-label -->
								<div class="frst-timeline-block frst-timeline-label-block" data-animation="slideInUp">
										<div class="frst-labels">
												<span>1979</span>
										</div>
										<!-- .frst-start-label -->
								</div>
								<!-- right-timeline-block -->
								<div class="frst-timeline-block frst-odd-item" data-animation="slideInUp">
										<div class="frst-timeline-img">
												<span><i class="fa fa-usd" aria-hidden="true"></i></span>
										</div>
										<!-- frst-timeline-img -->
										<div class="frst-timeline-content">
										 <div class="frst-timeline-content-inner">
														<img src="<?php echo site_url(); ?>/wp-content/themes/understrap-child/images/timeline/BWAY-115_Timeline_9_1979_Acquistion_icon.jpg" />
														<div class="text-content">
														<p>In 1979, Brockway Glass Co. acquires Standard Container as a wholly owned subsidiary.</p></div>
												</div>
												<!-- frst-timeline-content-inner -->
										</div>
										<!-- frst-timeline-content -->
								</div>
								<!-- next-year-label -->
								<div class="frst-timeline-block frst-timeline-label-block" data-animation="slideInUp">
										<div class="frst-labels frst-start-label">
												<span>1980</span>
										</div>
										<!-- .frst-start-label -->
								</div>
								<!-- left-timeline-block -->
								<div class="frst-timeline-block frst-even-item" data-animation="slideInUp">
										<div class="frst-timeline-img">
												<span><i class="fa fa-usd"></i></span>
										</div>
										<!-- frst-timeline-img -->
										<div class="frst-timeline-content">
												<div class="frst-timeline-content-inner">
														<img src="<?php echo site_url(); ?>/wp-content/themes/understrap-child/images/timeline/BWAY-115_Timeline_10_1980_Steel_Pail.jpg" />
														<div class="text-content">
														<p>In 1980, Brockway buys Inland Steel Container and expands steel pail production. </p></div>
												</div>
												<!-- frst-timeline-content-inner -->
										</div>
										<!-- frst-timeline-content -->
								</div>
								<!-- next-year-label -->
								<div class="frst-timeline-block frst-timeline-label-block" data-animation="slideInUp">
										<div class="frst-labels">
												<span>1985</span>
										</div>
										<!-- .frst-start-label -->
								</div>
								<!-- right-timeline-block -->
								<div class="frst-timeline-block frst-odd-item" data-animation="slideInUp">
										<div class="frst-timeline-img">
												<span><i class="fa fa-building-o" aria-hidden="true"></i></span>
										</div>
										<!-- frst-timeline-img -->
										<div class="frst-timeline-content">
										 <div class="frst-timeline-content-inner">
														<img src="<?php echo site_url(); ?>/wp-content/themes/understrap-child/images/timeline/BWAY-115_Timeline_11_1985_Brockway_Logo.jpg" />
														<div class="text-content">
														<p>In 1985, the company rebrands itself as Brockway Standard after renaming its business units to Brockway Glass, Brockway Plastics, Brockway Air, and Brockway Standard.</p></div>
												</div>
												<!-- frst-timeline-content-inner -->
										</div>
										<!-- frst-timeline-content -->
								</div>
								<!-- next-year-label -->
								<div class="frst-timeline-block frst-timeline-label-block" data-animation="slideInUp">
										<div class="frst-labels frst-start-label">
												<span>1989</span>
										</div>
										<!-- .frst-start-label -->
								</div>
								<!-- left-timeline-block -->
								<div class="frst-timeline-block frst-even-item" data-animation="slideInUp">
										<div class="frst-timeline-img">
												<span><i class="fa fa-usd"></i></span>
										</div>
										<!-- frst-timeline-img -->
										<div class="frst-timeline-content">
												<div class="frst-timeline-content-inner">
														<img src="<?php echo site_url(); ?>/wp-content/themes/understrap-child/images/timeline/BWAY-115_Timeline_12_1989_Acquistion_icon.jpg" />
														<div class="text-content">
														<p>In 1989, Brockway Standards Holding Co. is created when two partners purchase the Brockway Standard Can Division from Owens-Illinois Corp.</p></div>
												</div>
												<!-- frst-timeline-content-inner -->
										</div>
										<!-- frst-timeline-content -->
								</div>
								<!-- next-year-label -->
								<div class="frst-timeline-block frst-timeline-label-block" data-animation="slideInUp">
										<div class="frst-labels">
												<span>1993</span>
										</div>
										<!-- .frst-start-label -->
								</div>
								<!-- right-timeline-block -->
								<div class="frst-timeline-block frst-odd-item" data-animation="slideInUp">
										<div class="frst-timeline-img">
												<span><i class="fa fa-usd" aria-hidden="true"></i></span>
										</div>
										<!-- frst-timeline-img -->
										<div class="frst-timeline-content">
										 <div class="frst-timeline-content-inner">
														<img src="<?php echo site_url(); ?>/wp-content/themes/understrap-child/images/timeline/BWAY-115_Timeline_13_1993_Monotop_Cans.jpg" />
														<div class="text-content">
														<p>In 1993, the company enters the monotop can business after acquiring Ellisco, Inc.</p></div>
												</div>
												<!-- frst-timeline-content-inner -->
										</div>
										<!-- frst-timeline-content -->
								</div>
								<!-- next-year-label -->
								<div class="frst-timeline-block frst-timeline-label-block" data-animation="slideInUp">
										<div class="frst-labels frst-start-label">
												<span>1994</span>
										</div>
										<!-- .frst-start-label -->
								</div>
								<!-- left-timeline-block -->
								<div class="frst-timeline-block frst-even-item" data-animation="slideInUp">
										<div class="frst-timeline-img">
												<span><i class="fa fa-bitbucket-square"></i></span>
										</div>
										<!-- frst-timeline-img -->
										<div class="frst-timeline-content">
												<div class="frst-timeline-content-inner">
														<img src="<?php echo site_url(); ?>/wp-content/themes/understrap-child/images/timeline/BWAY-115_Timeline_14_1994_UN_Rated_Pails.jpg" />
														<div class="text-content">
														<p>In 1994, Brockway Standards Holding Co. develops and patents United Nations–rated steel pails and is the first to introduce them to the marketplace.</p></div>
												</div>
												<!-- frst-timeline-content-inner -->
										</div>
										<!-- frst-timeline-content -->
								</div>
								<!-- next-year-label -->
								<div class="frst-timeline-block frst-timeline-label-block" data-animation="slideInUp">
										<div class="frst-labels">
												<span>1996</span>
										</div>
										<!-- .frst-start-label -->
								</div>
								<!-- right-timeline-block -->
								<div class="frst-timeline-block frst-odd-item" data-animation="slideInUp">
										<div class="frst-timeline-img">
												<span><i class="fa fa-expand" aria-hidden="true"></i></span>
										</div>
										<!-- frst-timeline-img -->
										<div class="frst-timeline-content">
										 <div class="frst-timeline-content-inner">
														<img src="<?php echo site_url(); ?>/wp-content/themes/understrap-child/images/timeline/BWAY-115_Timeline_15_1996_BWAY_Logo.jpg" />
														<div class="text-content">
														<p>In 1996, the company is renamed BWAY Corp. BWAY continues to expand its product line through the acquisition of Milton Can Co. and the Davies Can Division of Crown Cork & Seal. BWAY enters the aerosol can business after buying Ball Corp.'s aerosol can line in Cincinnati, Ohio.</p></div>
												</div>
												<!-- frst-timeline-content-inner -->
										</div>
										<!-- frst-timeline-content -->
								</div>
								<!-- next-year-label -->
								<div class="frst-timeline-block frst-timeline-label-block" data-animation="slideInUp">
										<div class="frst-labels frst-start-label">
												<span>2004</span>
										</div>
										<!-- .frst-start-label -->
								</div>
								<!-- left-timeline-block -->
								<div class="frst-timeline-block frst-even-item" data-animation="slideInUp">
										<div class="frst-timeline-img">
												<span><i class="fa fa-expand"></i></span>
										</div>
										<!-- frst-timeline-img -->
										<div class="frst-timeline-content">
												<div class="frst-timeline-content-inner">
														<img src="<?php echo site_url(); ?>/wp-content/themes/understrap-child/images/timeline/BWAY-115_Timeline_8_2004_Plastic_Pails.jpg" />
														<div class="text-content">
														<p>In 2004, BWAY expands production of plastic products and adds 11 facilities across the U.S., Puerto Rico and Canada after buying SST Corp. and NAMPAC.</p></div>
												</div>
												<!-- frst-timeline-content-inner -->
										</div>
										<!-- frst-timeline-content -->
								</div>
								<!-- next-year-label -->
								<div class="frst-timeline-block frst-timeline-label-block" data-animation="slideInUp">
										<div class="frst-labels">
												<span>2006</span>
										</div>
										<!-- .frst-start-label -->
								</div>
								<!-- right-timeline-block -->
								<div class="frst-timeline-block frst-odd-item" data-animation="slideInUp">
										<div class="frst-timeline-img">
												<span><i class="fa fa-expand" aria-hidden="true"></i></span>
										</div>
										<!-- frst-timeline-img -->
										<div class="frst-timeline-content">
										 <div class="frst-timeline-content-inner">
														<img src="<?php echo site_url(); ?>/wp-content/themes/understrap-child/images/timeline/BWAY-115_Timeline_17_2006_Acquistion_icon.jpg" />
														<div class="text-content">
														<p>In 2006, BWAY expands in Canada after acquiring ICL and Vulcan Containers.</p></div>
												</div>
												<!-- frst-timeline-content-inner -->
										</div>
										<!-- frst-timeline-content -->
								</div>
								<!-- next-year-label -->
								<div class="frst-timeline-block frst-timeline-label-block" data-animation="slideInUp">
										<div class="frst-labels frst-start-label">
												<span>2009</span>
										</div>
										<!-- .frst-start-label -->
								</div>
								<!-- left-timeline-block -->
								<div class="frst-timeline-block frst-even-item" data-animation="slideInUp">
										<div class="frst-timeline-img">
												<span><i class="fa fa-expand"></i></span>
										</div>
										<!-- frst-timeline-img -->
										<div class="frst-timeline-content">
												<div class="frst-timeline-content-inner">
														<img src="<?php echo site_url(); ?>/wp-content/themes/understrap-child/images/timeline/BWAY-115_Timeline_18_2009_Hybrid.jpg" />
														<div class="text-content">
														<p>In 2009, the company enters the hybrid paint can business and expands production of metal paint cans, steel pails and plastic open head pails through acquisition of Central Can Co. and Ball Corp.'s Newnan, Georgia plant.</p></div>
												</div>
												<!-- frst-timeline-content-inner -->
										</div>
										<!-- frst-timeline-content -->
								</div>
								<!-- next-year-label -->
								<div class="frst-timeline-block frst-timeline-label-block" data-animation="slideInUp">
										<div class="frst-labels">
												<span>2010</span>
										</div>
										<!-- .frst-start-label -->
								</div>
								<!-- right-timeline-block -->
								<div class="frst-timeline-block frst-odd-item" data-animation="slideInUp">
										<div class="frst-timeline-img">
												<span><i class="fa fa-usd" aria-hidden="true"></i></span>
										</div>
										<!-- frst-timeline-img -->
										<div class="frst-timeline-content">
										 <div class="frst-timeline-content-inner">
														<img src="<?php echo site_url(); ?>/wp-content/themes/understrap-child/images/timeline/BWAY-115_Timeline_19_2010_Pails_Drums.jpg" />
														<div class="text-content">
														<p>In 2010, BWAY acquires Plastican Inc., a leading manufacturer of open-head plastic pails, screw-top pails and drums.</p></div>
												</div>
												<!-- frst-timeline-content-inner -->
										</div>
										<!-- frst-timeline-content -->
								</div>
								<!-- next-year-label -->
								<div class="frst-timeline-block frst-timeline-label-block" data-animation="slideInUp">
										<div class="frst-labels frst-start-label">
												<span>2013</span>
										</div>
										<!-- .frst-start-label -->
								</div>
								<!-- left-timeline-block -->
								<div class="frst-timeline-block frst-even-item" data-animation="slideInUp">
										<div class="frst-timeline-img">
												<span><i class="fa fa-usd"></i></span>
										</div>
										<!-- frst-timeline-img -->
										<div class="frst-timeline-content">
												<div class="frst-timeline-content-inner">
														<img src="<?php echo site_url(); ?>/wp-content/themes/understrap-child/images/timeline/BWAY-115_Timeline_20_2013_2013_Ropak.jpg" />
														<div class="text-content">
														<p>In 2013, BWAY buys Ropak Packaging, a leading producer of rigid, plastic containers for consumer goods, food, dairy products, petroleum and paint.</p></div>
												</div>
												<!-- frst-timeline-content-inner -->
										</div>
										<!-- frst-timeline-content -->
								</div>
								<!-- next-year-label -->
								<div class="frst-timeline-block frst-timeline-label-block" data-animation="slideInUp">
										<div class="frst-labels">
												<span>2016</span>
										</div>
										<!-- .frst-start-label -->
								</div>
								<!-- right-timeline-block -->
								<div class="frst-timeline-block frst-odd-item" data-animation="slideInUp">
										<div class="frst-timeline-img">
												<span><i class="fa fa-expand" aria-hidden="true"></i></span>
										</div>
										<!-- frst-timeline-img -->
										<div class="frst-timeline-content">
										 <div class="frst-timeline-content-inner">
														<img src="<?php echo site_url(); ?>/wp-content/themes/understrap-child/images/timeline/BWAY-115_Timeline_21_2016_KLW.jpg" />
														<div class="text-content">
														<p>In 2016, BWAY expands production of plastic, tight-head containers by acquiring KLW Plastics.</p></div>
												</div>
												<!-- frst-timeline-content-inner -->
										</div>
										<!-- frst-timeline-content -->
								</div>
								<!-- next-year-label -->
								<div class="frst-timeline-block frst-timeline-label-block" data-animation="slideInUp">
										<div class="frst-labels frst-start-label">
												<span>2017</span>
										</div>
										<!-- .frst-start-label -->
								</div>
								<!-- left-timeline-block -->
								<div class="frst-timeline-block frst-even-item" data-animation="slideInUp">
										<div class="frst-timeline-img">
												<span><i class="fa fa-usd"></i></span>
										</div>
										<!-- frst-timeline-img -->
										<div class="frst-timeline-content">
												<div class="frst-timeline-content-inner">
														<img src="<?php echo site_url(); ?>/wp-content/themes/understrap-child/images/timeline/BWAY-115_Timeline_22_2017_F_Style_Pails.jpg" />
														<div class="text-content">
														<p>In 2017, the company adds capacity in the paint and general line markets after buying a Ball Corp. plant in Hubbard, Ohio. </p></div>
												</div>
												<!-- frst-timeline-content-inner -->
										</div>
										<!-- frst-timeline-content -->
								</div>
						</div>
						<!-- frst-timeline -->
				</div>
				<!-- .frst-container -->
			</div>

		</div>

	</div><!-- .entry-content -->

</article><!-- #post-## -->
