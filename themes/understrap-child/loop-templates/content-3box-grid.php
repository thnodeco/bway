<?php
/**
 * Partial template for content in 3box-grid.php
 *
 * @package understrap
 */

$upload_dir = wp_upload_dir();

?>

<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">

	<div class="container-fluid">
		<div class="row justify-content-center pg-headers" style="background: url('<?php the_field('header_image'); ?>') no-repeat center center;">
		</div>
	</div>

	<div class="entry-content gray-angle-background" style="height: 100%;">

		<div class="container mb-7">
				<?php the_content(); ?>
		</div>

		<div class="container">
			<?php if ( is_page([1894,923,901,906]) ) { ?>
				<div class="row justify-content-center">
			<?php } else { ?>
			<div class="row justify-content-center">
			<?php } ?>
			<?php if( have_rows('boxes') ): ?>
				<?php while( have_rows('boxes') ): the_row();

					// vars
					$title = get_sub_field('title');
					$image = get_sub_field('image');
					$caption = get_sub_field('caption');
					$link = get_sub_field('linked_page');
					$manual_link = get_sub_field('manual_link');
					$color =  get_sub_field('color')

					?>

						<?php if ( is_page([1894,923,901,906]) ) { ?>
							<a href="<?php echo $link; ?><?php echo $manual_link; ?>" alt="<?php echo $title; ?>" class="industrial-hover" style="display:block">
							<div class="justify-content-center">
								<div class="overlay" style="background: url('<?php echo $image; ?>') no-repeat center center; min-height: 329px;">
									<div class="grid-title-lg" style="">
										<h2 class="text-uppercase"><?php echo $title; ?></h2>
									</div>
								</div>
								<p style="border-bottom: 10px solid <?php echo $color; ?>;" class="text-center learn-more"><?php echo $caption; ?></p>
							</div>
						</a>
						<?php } else { ?>
						<a href="<?php echo $link; ?><?php echo $manual_link; ?>" alt="<?php echo $title; ?>" class="industrial-hover">
						<div class="ml-md-1 mr-md-1 mb-md-5">
								<div class="endmarket" style="background-image: url('<?php echo $image; ?>');">
								<div class="overlay">
									<div class="grid-title" style="">
										<h2 class="text-uppercase"><?php echo $title; ?></h2>
									</div>
								</div>
							</div>
							<p style="border-bottom: 10px solid <?php echo $color; ?>;" class="text-center learn-more"><?php echo $caption; ?></p>
						</div>
						</a>
						<?php } ?>

				<?php endwhile; ?>
			<?php endif; ?>
		</div>
		</div>

		<?php if ( is_page('industrial-markets') ) { ?>
			<div class="container-fluid">
				<div class="row justify-content-center mt-4">
					<h2 style="color:#004b8d; text-align: center">Looking for institutional markets?</h2>
				</div>
				<div class="row justify-content-center">
					<p><a href="<?php echo site_url(); ?>/industrial-markets/institutional/" class="btn-blue" style="font-size: 1.25rem;">Yes, Show Me Now</a></p>
				</div>
			</div>
		 <?php } ?>

		<div class="container-fluid">
			<div class="row justify-content-center" style="margin: 100px 0;">
				<div class="col-sm-12 col-md-8 col-lg-6">
					<h4 style="color: #58595b; text-align: center; font-family: DIN-Medium; font-weight: bold; text-transform:uppercase">Have Questions<br />About Our Products?</h4>
					<div class="container">
						<div class="row justify-content-center mt-5">
							<div class="col-sm-3 col-md-3">
								<center><a href="tel:8005272267" title="Call Us at 800.527.2267"><img src="<?php echo $upload_dir['baseurl']; ?>/2017/07/phone-icon.png" /></center><br />
								<p style="text-align: center; font-weight: bold;">800.527.2267</a></p>
							</div>
							<div class="col-sm-3 col-md-3">
								<center><a href="mailto:sales@bwaycorp.com?subject=Product%20Question" title="Contact Us"><img src="<?php echo $upload_dir['baseurl']; ?>/2017/07/email-icon.png" /></center><br />
								<p style="text-align: center; font-weight: bold;">EMAIL</a></p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div><!-- .entry-content -->

</article><!-- #post-## -->
