<?php
/**
 * Single post partial template.
 *
 * @package understrap
 */

?>

<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">

	<header class="entry-header">

		<div class="container">
			<div class="row justify-content-center" style="margin: 40px 0">
				<h1 class="entry-title" style="color: #004b8d; font-weight: bold; text-transform: uppercase;">Leadership</h1>
			</div>
		</div>

	</header><!-- .entry-header -->

	<div class="container-fluid">
		<div class="row mb-5">
			<div class="col-md-2">
				<center><img src="<?php echo get_the_post_thumbnail_url( $post->ID, 'full' ); ?>" alt="<?php echo $name ?>" style="border: 2px solid #ececec; margin-bottom: 10px;"/><br />
				<a href="<?php the_permalink(); ?>" style="font-weight:bold;"><?php the_title(); ?></a><br />
				<span style="display: inline-block; font-size: 12px; color: #797979"><?php the_field('title'); ?></span></center>
			</div>
			<div class="col-md-10 mt-5">
				<?php the_content(); ?>
			</div>

		</div>
		<center><a href="<?php echo site_url(); ?>/leadership" class="btn btn-primary">Back to Leadership Overview</a></center>
		<br /><br /><br /><br />
	</div>
</article><!-- #post-## -->
