<?php
/**
 * Partial template for content in 2box-grid.php
 *
 * @package understrap
 */

$upload_dir = wp_upload_dir();

?>

<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">

	<div class="container-fluid">
		<div class="row justify-content-center pg-headers" style="background: url('<?php the_field('header_image'); ?>') no-repeat center center;">
		</div>
	</div>

	<div class="entry-content ">

		<div class="container mb-7">
				<?php the_content(); ?>
		</div>

		<div class="container">
			<div class="row mb-6">
			<?php if( have_rows('boxes') ): ?>
				<?php while( have_rows('boxes') ): the_row();

					// vars
					$title = get_sub_field('title');
					$image = get_sub_field('image');
					$caption = get_sub_field('caption');
					$link = get_sub_field('link');

					?>

						<div class="col-md-5 ml-md-5">
							<a href="<?php echo $link; ?>" title="<?php echo $title; ?>" style="display: block">
								<div class="overlay" style="background: url('<?php echo $image; ?>') no-repeat center center; max-height: 329px;">
									<div class="stories-title" style="cursor:pointer; position: relative; display: block;">
										<h2 class="text-uppercase"><?php echo $title; ?></h2>
									</div>
								</div>
							</a>
							<p style="border-bottom: 10px solid #7d7d7d; padding: 20px; margin-bottom: 90px; text-transform: uppercase" class="text-center"><a href="<?php echo $link; ?>" title="<?php echo $title; ?>" style="color: #333"><?php echo $caption; ?></a></p>
						</div>

				<?php endwhile; ?>
			<?php endif; ?>
			</div>
		</div>

		<div class="container-fluid">
			<div class="row justify-content-center" style="margin: 100px 0;">
				<div class="col-sm-12 col-md-8 col-lg-6">
					<h4 style="color: #58595b; text-align: center; font-family: DIN-Medium; font-weight: bold; text-transform:uppercase">Have Questions<br />About Our Products?</h4>
					<div class="container">
						<div class="row justify-content-center mt-5">
							<div class="col-sm-3 col-md-3">
								<center><a href="tel:8005272267" title="Call Us at 800.527.2267"><img src="<?php echo $upload_dir['baseurl']; ?>/2017/07/phone-icon.png" /></center><br />
								<p style="text-align: center; font-weight: bold;">800.527.2267</a></p>
							</div>
							<div class="col-sm-3 col-md-3">
								<center><a href="mailto:sales@bwaycorp.com?subject=Product%20Question" title="Contact Us"><img src="<?php echo $upload_dir['baseurl']; ?>/2017/07/email-icon.png" /></center><br />
								<p style="text-align: center; font-weight: bold;">EMAIL</a></p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div><!-- .entry-content -->

</article><!-- #post-## -->
