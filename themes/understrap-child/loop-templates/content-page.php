<?php
/**
 * Partial template for content in full-contained.php
 *
 * @package understrap
 */

?>
<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">

	<?php if ( get_field( 'header_image' ) ): ?>

		<div class="container-fluid">
			<div class="row justify-content-center pg-headers" style="background: url('<?php the_field('header_image'); ?>') no-repeat center center;">
			</div>
		</div>

	<?php else: ?><?php endif; ?>

	<div class="entry-content <?php if ( is_page([2891,2325,3427]) ) {} else { ?> gray-angle-background	<?php } ?>" style="height: 100%;">

		<div class="container mb-5">
				<?php the_content(); ?>
		</div>

	</div><!-- .entry-content -->

</article><!-- #post-## -->
