<?php
/**
 * Partial template for content in page.php
 *
 * @package understrap
 */

$upload_dir = wp_upload_dir();
?>

<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">

	<div class="entry-content">

		<?php the_content(); ?>

		<div class="row justify-content-end mt-5 mb-5" style="background: url('https://bwaycorp.com/wp-content/uploads/2017/09/BWAY-130_Industrial_Consumer_4000x550_1.png') no-repeat center center; position: relative; background-size: cover; min-height: 400px;">
			<div class="col-12 col-md-6 mt-md-5 pt-3 pt-md-5">
				<div class="col-12 col-md-9 col-lg-8 col-xl-5 offset-md-0 offset-lg-2 offset-xl-6">
					<h2 style="color: #638b18; text-transform: uppercase; text-align: right;">Industrial<br />Packaging</h2>
					<p style="color: #ffffff; font-size: 16px; font-weight: bold; text-align: right;">Every industrial product needs durable, reliable packaging. With BWAY as your one-stop industrial packaging provider, sourcing, scalability and growth are simplified.<br />
					<a href="industrial-markets" class="btn" style="background: transparent; color:#fff;font-weight:normal;">Learn More</a></p>
				</div>
			</div>
			<div class="col-12 col-md-6 mt-md-5 pt-md-5">
				<div class="col-12 col-md-10 col-lg-8 col-xl-5 offset-md-1">
					<h2 style="color: orange; text-transform: uppercase;">Consumer Goods<br />Packaging</h2>
					<p style="color: #ffffff; font-size: 16px; font-weight: bold;">The world's most recognized consumer brands trust BWAY to provide innovative packaging solutions. See how we can help you win space, both on store shelves and in consumers' hearts.<br />
					<a href="consumer-markets" class="btn" style="background: transparent; color:#fff;font-weight:normal;">Learn More</a></p>
				</div>
			</div>
		</div>

<div class="container" style="width: 100%; padding-left: 0; padding-right: 0;">
		<div class="row" style="overflow: hidden; position:relative; margin-top: -2px;">
			<div class="explorewrap">
					<h1 style="color:#fff;">EXPLORE AND DISCOVER</h1>
					<center><p style="color:#fff; font-size: 1.2rem">Browse our product catalog to see just<br />how far and wide our products span.</p>
					<a href="product-category/plastic-products/" class="btn" style="background-color: #00b1ac;color:#fff;border: none;font-weight: normal;">PLASTIC</a><a href="product-category/metal-products/" class="btn" style="background-color: #638b18;color:#fff;border: none;font-weight: normal;margin-left: 30px;">METAL</a></center>
			</div>
			<div id="slider" class="col col-md-12 flexslider explore-background hidden-md-down">
			  <ul class="slides topslide">

					<li style="max-width: 1920px;"><img src="<?php echo $upload_dir['baseurl']; ?>/2017/09/open_head_pail_4-9_gal.png" /></li>
					<li style="max-width: 1920px;"><img src="<?php echo $upload_dir['baseurl']; ?>/2017/09/tight_head_container_3_gal.png" /></li>
					<li style="max-width: 1920px;"><img src="<?php echo $upload_dir['baseurl']; ?>/2017/09/twist_lock.png" /></li>
					<li style="max-width: 1920px;"><img src="<?php echo $upload_dir['baseurl']; ?>/2017/09/bento.png" /></li>
					<li style="max-width: 1920px;"><img src="<?php echo $upload_dir['baseurl']; ?>/2017/09/screw_top_pail_3-5_gal.png" /></li>
					<li style="max-width: 1920px;"><img src="<?php echo $upload_dir['baseurl']; ?>/2017/09/plastic_drums.png" /></li>
					<li style="max-width: 1920px;"><img src="<?php echo $upload_dir['baseurl']; ?>/2017/09/dairy_container_2-5_gal.png" /></li>
					<li style="max-width: 1920px;"><img src="<?php echo $upload_dir['baseurl']; ?>/2017/09/1ltr_open_head_pail.png" /></li>
					<li style="max-width: 1920px;"><img src="<?php echo $upload_dir['baseurl']; ?>/2017/09/f-style_gallon.png" /></li>
					<li style="max-width: 1920px;"><img src="<?php echo $upload_dir['baseurl']; ?>/2017/09/cone_pour_top_can.png" /></li>
					<li style="max-width: 1920px;"><img src="<?php echo $upload_dir['baseurl']; ?>/2017/09/ammunition_boxes.png" /></li>
					<li style="max-width: 1920px;"><img src="<?php echo $upload_dir['baseurl']; ?>/2017/09/aerosol_necked.png" /></li>
					<li style="max-width: 1920px;"><img src="<?php echo $upload_dir['baseurl']; ?>/2017/09/steel_open_head_pail_5_gal.png" /></li>
					<li style="max-width: 1920px;"><img src="<?php echo $upload_dir['baseurl']; ?>/2017/09/Steel_Tight_Head_Pail.png" /></li>
					<li style="max-width: 1920px;"><img src="<?php echo $upload_dir['baseurl']; ?>/2017/09/oil_can.png" /></li>
					<li style="max-width: 1920px;"><img src="<?php echo $upload_dir['baseurl']; ?>/2017/09/monotop_can_211x304_1-75_drop.png" /></li>
					<li style="max-width: 1920px;"><img src="<?php echo $upload_dir['baseurl']; ?>/2017/09/metal_paint_can.png" /></li>
					<li style="max-width: 1920px;"><img src="<?php echo $upload_dir['baseurl']; ?>/2017/09/hybrid.png" /></li>

			  </ul>
			</div>
				<div id="carousel" class="col col-md-8 flexslider thumbs">
				  <ul class="slides">

						<li style="max-width: 145px;"><img src="<?php echo $upload_dir['baseurl']; ?>/2017/09/Open_Head_Pail_49_gal_Drop.jpg" /><br /><div class="thmb-caption">Open Head Pails</div></li>
						<li style="max-width: 145px;"><img src="<?php echo $upload_dir['baseurl']; ?>/2017/09/Tight_Head_Container_5_gal_Drop.png" /><br /><div class="thmb-caption">Plastic Tight Head Containers</div></li>
						<li style="max-width: 145px;"><img src="<?php echo $upload_dir['baseurl']; ?>/2017/09/twist_lock-1.png" /><br /><div class="thmb-caption">Twist and Lock Pails</div></li>
						<li style="max-width: 145px;"><img src="<?php echo $upload_dir['baseurl']; ?>/2017/09/bento_square_ikura.png" /><br /><div class="thmb-caption">Fish/Misc. Products</div></li>
						<li style="max-width: 145px;"><img src="<?php echo $upload_dir['baseurl']; ?>/2017/09/Screw_Top_Pail_35_gal_Drop.png" /><br /><div class="thmb-caption">Screw Top Pails</div></li>
						<li style="max-width: 145px;"><img src="<?php echo $upload_dir['baseurl']; ?>/2017/09/PlasticDrums_Drop.png" /><br /><div class="thmb-caption">Plastic Drums</div></li>
						<li style="max-width: 145px;"><img src="<?php echo $upload_dir['baseurl']; ?>/2017/09/Dairy-Container_25-gal-Ice-Cream_Drop.png" /><br /><div class="thmb-caption">Dairy Containers</div></li>
						<li style="max-width: 145px;"><img src="<?php echo $upload_dir['baseurl']; ?>/2017/09/1L-PP-with-cover_002-lo.png" /><br /><div class="thmb-caption">Plastic Open Head Pails</div></li>
						<li style="max-width: 145px;"><img src="<?php echo $upload_dir['baseurl']; ?>/2017/09/F-Style_Gallon_Drop.png" /><br /><div class="thmb-caption">F-Style Containers</div></li>
						<li style="max-width: 145px;"><img src="<?php echo $upload_dir['baseurl']; ?>/2017/09/Cone_Pour_Top_Can_307x704_Beta-Cone_Drop.png" /><br /><div class="thmb-caption">Cone and Pour Top Cans</div></li>
						<li style="max-width: 145px;"><img src="<?php echo $upload_dir['baseurl']; ?>/2017/09/Ammunition_Boxes_Drop.png" /><br /><div class="thmb-caption">Ammunition Boxes</div></li>
						<li style="max-width: 145px;"><img src="<?php echo $upload_dir['baseurl']; ?>/2017/09/Aerosol_Necked_In_207.png" /><br /><div class="thmb-caption">Aerosol Cans</div></li>
						<li style="max-width: 145px;"><img src="<?php echo $upload_dir['baseurl']; ?>/2017/09/Steel_Open_Head_Pail_5_ga_Drop.png" /><br /><div class="thmb-caption">Steel Open Head Pails</div></li>
						<li style="max-width: 145px;"><img src="<?php echo $upload_dir['baseurl']; ?>/2017/09/Steel_Tight_Head_Pails_5_Gallons.jpg" /><br /><div class="thmb-caption">Steel Tight Head Pails</div></li>
						<li style="max-width: 145px;"><img src="<?php echo $upload_dir['baseurl']; ?>/2017/09/Oil_Can_Drop.png" /><br /><div class="thmb-caption">Oil Cans</div></li>
						<li style="max-width: 145px;"><img src="<?php echo $upload_dir['baseurl']; ?>/2017/09/Monotop_Can_211x304_175_Drop.png" /><br /><div class="thmb-caption">Monotop Cans</div></li>
						<li style="max-width: 145px;"><img src="<?php echo $upload_dir['baseurl']; ?>/2017/09/Metal_Paint_Can_1_gal.png" /><br /><div class="thmb-caption">Metal Paint Cans</div></li>
						<li style="max-width: 145px;"><img src="<?php echo $upload_dir['baseurl']; ?>/2017/09/Hybrid-Quart-Can-hi_Drop.png" /><br /><div class="thmb-caption">Hybrid Paint Cans</div></li>

				  </ul>
				</div>

		</div>
</div>

	<div class="row hidden-xl-up" style="margin-top: 60px;">
		<div class="col-md-12 text-center">
			<div class="map-nav">
				<center><h1>Everywhere You Are</h1>
				<p>In addition to its <strong>26 manufacturing facilities,</strong> BWAY partners with numerous distributors across the U.S., Canada, Mexico and the Caribbean. No matter where you are, we're never far.</p></center>
				<div class="row" style="margin-top: 20px;">
					<div class="col-12 col-sm-4 text-center">
						<span class="US d-block">United States</span>

						<a data-toggle="collapse" href="#executiveoffice" aria-expanded="false" aria-controls="executiveoffice" style="display: block"><span class="blue d-block">Executive Office</span></a>
						<div class="collapse" id="executiveoffice">
							<div class="card card-block" style="padding: .75rem">
								<p style="margin-bottom: 0;">
									1515 W. 22nd St.<br />
									Suite 1100<br />
									Oak Brook, Illinois 60523<br />
									<span class="blue">P:</span> (630) 203-4100<br />
									<span class="blue">F:</span> (630) 203-4110<br />
								</p>
						  </div>
						</div>

						<a data-toggle="collapse" href="#corporateoffice" aria-expanded="false" aria-controls="corporateoffice" style="display: block"><span class="blue d-block">Corporate Office</span></a>
						<div class="collapse" id="corporateoffice">
							<div class="card card-block" style="padding: .75rem">
								<p style="margin-bottom: 0;">
									8607 Roberts Drive <br />
									Suite 250<br />
									Atlanta, Georgia 30350<br />
									<span class="blue">P:</span> (770) 645-4800<br />
									<span class="blue">F:</span> (770) 645-4810<br />
								</p>
						  </div>
						</div>

						<a data-toggle="collapse" href="#chicagolist" aria-expanded="false" aria-controls="chicagolist" style="display: block"><span class="blue d-block">Chicago</span></a>
						<div class="collapse" id="chicagolist">
							<div class="card card-block" style="padding: .75rem">
								<p style="margin-bottom: 0;">
									3200 S. Kilbourn Ave.<br />
									Chicago, Illinois 60623<br />
									<span class="blue">P:</span> (773) 890-3300<br />
									<span class="blue">F:</span> (773) 254-4759<br />
									<span class="blue">Products:</span> Hybrid paint cans, metal food cans, metal paint cans, steel open head pails, steel tight head pails
								</p>
						  </div>
						</div>

						<a data-toggle="collapse" href="#cincinnatilist" aria-expanded="false" aria-controls="cincinnatilist" style="display: block"><span class="blue d-block">Cincinnati</span></a>
						<div class="collapse" id="cincinnatilist">
							<div class="card card-block" style="padding: .75rem">
								<p style="margin-bottom: 0;">
									8200 Broadwell Road<br />
									Cincinnati, Ohio 45244<br />
									<span class="blue">P:</span> (513) 388-2200<br />
									<span class="blue">F:</span> (513) 388-2215<br />
									<span class="blue">Products:</span> Aerosol cans
								</p>
						  </div>
						</div>

						<a data-toggle="collapse" href="#fontanalist" aria-expanded="false" aria-controls="fontanalist" style="display: block"><span class="blue d-block">Fontana</span></a>
						<div class="collapse" id="fontanalist">
							<div class="card card-block" style="padding: .75rem">
								<p style="margin-bottom: 0;">
									11440 Pacific Ave.<br />
									Fontana, California 92337<br />
									<span class="blue">P:</span> (951) 361-4100<br />
									<span class="blue">F:</span> (951) 361-4110<br />
									<span class="blue">Products:</span> metal paint cans
								</p>
						  </div>
						</div>

						<a data-toggle="collapse" href="#garlandlist" aria-expanded="false" aria-controls="garlandlist" style="display: block"><span class="blue d-block">Garland</span></a>
						<div class="collapse" id="garlandlist">
							<div class="card card-block" style="padding: .75rem">
								<p style="margin-bottom: 0;">
									3737 Miller Park Drive<br />
									Garland, Texas 75042<br />
									<span class="blue">P:</span> (972) 535-1100<br />
									<span class="blue">F:</span> (972) 535-1100<br />
									<span class="blue">Products:</span> F-style containers, metal paint cans
								</p>
						  </div>
						</div>

						<a data-toggle="collapse" href="#homervillelist" aria-expanded="false" aria-controls="homervillelist" style="display: block"><span class="blue d-block">Homerville</span></a>
						<div class="collapse" id="homervillelist">
							<div class="card card-block" style="padding: .75rem">
								<p style="margin-bottom: 0;">
									1601 Valdosta Highway<br />
									Homerville, Georgia 31634<br />
									<span class="blue">P:</span> (912) 487-4000<br />
									<span class="blue">F:</span> (912) 487-4010<br />
									<span class="blue">Products:</span> Ammunition boxes, cone and pour top cans, metal paint cans, monotop cans, steel open head pails, steel tight head pails, oil cans
								</p>
						  </div>
						</div>

						<a data-toggle="collapse" href="#memphislist" aria-expanded="false" aria-controls="memphislist" style="display: block"><span class="blue d-block">Memphis</span></a>
						<div class="collapse" id="memphislist">
							<div class="card card-block" style="padding: .75rem">
								<p style="margin-bottom: 0;">
									4651 Hickory Hill Road<br />
									Suite 104<br />
									Memphis, Tennessee 38141<br />
									<span class="blue">P:</span> (901) 251-2800<br />
									<span class="blue">F:</span> (901) 251-2810<br />
									<span class="blue">Products:</span> F-style containers
								</p>
						  </div>
						</div>

						<a data-toggle="collapse" href="#sturtevantlist" aria-expanded="false" aria-controls="sturtevantlist" style="display: block"><span class="blue d-block">Sturtevant</span></a>
						<div class="collapse" id="sturtevantlist">
							<div class="card card-block" style="padding: .75rem">
								<p style="margin-bottom: 0;">
									10277 Venice Drive<br />
									Sturtevant, Wisconsin 53177<br />
									<span class="blue">P:</span> (262) 417-1201<br />
									<span class="blue">F:</span> (262) 417-1210<br />
									<span class="blue">Products:</span> Aerosol cans
								</p>
						  </div>
						</div>

						<a data-toggle="collapse" href="#trentonlist" aria-expanded="false" aria-controls="trentonlist" style="display: block"><span class="blue d-block">Trenton</span></a>
						<div class="collapse" id="trentonlist">
							<div class="card card-block" style="padding: .75rem">
								<p style="margin-bottom: 0;">
									Six Litho Road<br />
									Trenton, New Jersey 08638<br />
									<span class="blue">P:</span> (732) 997-4050<br />
									<span class="blue">F:</span> (732) 997-4055<br />
									<span class="blue">Products:</span> steel open head pails, steel tight head pails
								</p>
						  </div>
						</div>

						<a data-toggle="collapse" href="#yorklist" aria-expanded="false" aria-controls="yorklist" style="display: block"><span class="blue d-block">York</span></a>
						<div class="collapse" id="yorklist">
							<div class="card card-block" style="padding: .75rem">
								<p style="margin-bottom: 0;">
									599 Davies Drive<br />
									York, Pennsylvania 17402<br />
									<span class="blue">P:</span> (717) 840-2100<br />
									<span class="blue">F:</span> (717) 840-2110<br />
									<span class="blue">Products:</span> Hybrid paint cans, metal paint cans, oil cans
								</p>
						  </div>
						</div>

						<a data-toggle="collapse" href="#bryanlist" aria-expanded="false" aria-controls="bryanlist" style="display: block"><span class="blue d-block">Bryan</span></a>
						<div class="collapse" id="bryanlist">
							<div class="card card-block" style="padding: .75rem">
								<p style="margin-bottom: 0;">
									1591 N. Harvey Mitchell Parkway<br />
									Bryan, Texas 77803<br />
									<span class="blue">P:</span> (979) 779-5900<br />
									<span class="blue">F:</span> (979) 823-1358<br />
									<span class="blue">Products:</span> Plastic open head pails, plastic tight head containers
								</p>
						  </div>
						</div>

						<a data-toggle="collapse" href="#cedarcitylist" aria-expanded="false" aria-controls="cedarcitylist" style="display: block"><span class="blue d-block">Cedar City</span></a>
						<div class="collapse" id="cedarcitylist">
							<div class="card card-block" style="padding: .75rem">
								<p style="margin-bottom: 0;">
									1033 N. Production Road<br />
									Cedar City, Utah 84721<br />
									<span class="blue">P:</span> (435) 865-1992<br />
									<span class="blue">F:</span> (435) 865-1993<br />
									<span class="blue">Products:</span> Plastic open head pails, plastic tight head containers, twist and lock pails
								</p>
						  </div>
						</div>

						<a data-toggle="collapse" href="#daytonlist" aria-expanded="false" aria-controls="daytonlist" style="display: block"><span class="blue d-block">Dayton</span></a>
						<div class="collapse" id="daytonlist">
							<div class="card card-block" style="padding: .75rem">
								<p style="margin-bottom: 0;">
									7 Wheeling Road<br />
									Dayton, New Jersey 08810<br />
									<span class="blue">P:</span> (732) 997-4100<br />
									<span class="blue">F:</span> (609) 860-0754<br />
									<span class="blue">Products:</span> Plastic open head pails, plastic tight head containers
								</p>
						  </div>
						</div>

						<a data-toggle="collapse" href="#elkgrove1list" aria-expanded="false" aria-controls="elkgrove1list" style="display: block"><span class="blue d-block">Elk Grove - Arthur Ave.</span></a>
						<div class="collapse" id="elkgrove1list">
							<div class="card card-block" style="padding: .75rem">
								<p style="margin-bottom: 0;">
									1350 Arthur Ave.<br />
									Elk Grove Village, Illinois 60007<br />
									<span class="blue">P:</span> (847) 956-0750<br />
									<span class="blue">F:</span> (847) 956-0756<br />
									<span class="blue">Products:</span> Plastic open head pails
								</p>
						  </div>
						</div>

						<a data-toggle="collapse" href="#elkgrove2list" aria-expanded="false" aria-controls="elkgrove2list" style="display: block"><span class="blue d-block">Elk Grove - Lively Blvd.</span></a>
						<div class="collapse" id="elkgrove2list">
							<div class="card card-block" style="padding: .75rem">
								<p style="margin-bottom: 0;">
									2350 Lively Blvd.<br />
									Elk Grove Village, Illinois 60007<br />
									<span class="blue">P:</span> (847) 860-4301<br />
									<span class="blue">F:</span> (847) 860-8197<br />
									<span class="blue">Products:</span> Plastic open head pails
								</p>
						  </div>
						</div>

						<a data-toggle="collapse" href="#indianapolislist" aria-expanded="false" aria-controls="indianapolislist" style="display: block"><span class="blue d-block">Indianapolis</span></a>
						<div class="collapse" id="indianapolislist">
							<div class="card card-block" style="padding: .75rem">
								<p style="margin-bottom: 0;">
									6061 Guion Road<br />
									Indianapolis, Indiana 46254<br />
									<span class="blue">P:</span> (317) 298-6155<br />
									<span class="blue">F:</span> (317) 298-6153<br />
									<span class="blue">Products:</span> Plastic tight head containers, plastic drums
								</p>
						  </div>
						</div>

						<a data-toggle="collapse" href="#lagrangelist" aria-expanded="false" aria-controls="lagrangelist" style="display: block"><span class="blue d-block">LaGrange</span></a>
						<div class="collapse" id="lagrangelist">
							<div class="card card-block" style="padding: .75rem">
								<p style="margin-bottom: 0;">
									1603 Orchard Hill Road<br />
									LaGrange, Georgia 30240<br />
									<span class="blue">P:</span> (706) 885-1772<br />
									<span class="blue">F:</span> (706) 812-9586<br />
									<span class="blue">Products:</span> Plastic open head pails, plastic tight head containers
								</p>
						  </div>
						</div>

						<a data-toggle="collapse" href="#leominsterlist" aria-expanded="false" aria-controls="leominsterlist" style="display: block"><span class="blue d-block">Leominster</span></a>
						<div class="collapse" id="leominsterlist">
							<div class="card card-block" style="padding: .75rem">
								<p style="margin-bottom: 0;">
									196 Industrial Road<br />
									Leominster, Massachusetts 01453<br />
									<span class="blue">P:</span> (978) 537-4911<br />
									<span class="blue">F:</span> (978) 537-6376<br />
									<span class="blue">Products:</span> Plastic open head pails, twist and lock pails
								</p>
						  </div>
						</div>

						<a data-toggle="collapse" href="#mansfieldlist" aria-expanded="false" aria-controls="mansfieldlist" style="display: block"><span class="blue d-block">Mansfield</span></a>
						<div class="collapse" id="mansfieldlist">
							<div class="card card-block" style="padding: .75rem">
								<p style="margin-bottom: 0;">
									1501 E. Dallas St.<br />
									Mansfield, Texas 76063<br />
									<span class="blue">P:</span> (817) 473-0259<br />
									<span class="blue">F:</span> (817) 473-8185<br />
									<span class="blue">Products:</span> Plastic open head pails, plastic tight head containers
								</p>
						  </div>
						</div>

						<a data-toggle="collapse" href="#monroelist" aria-expanded="false" aria-controls="monroelist" style="display: block"><span class="blue d-block">Monroe</span></a>
						<div class="collapse" id="monroelist">
							<div class="card card-block" style="padding: .75rem">
								<p style="margin-bottom: 0;">
									980 Deneen Ave.<br />
									Monroe, Ohio 45050<br />
									<span class="blue">P:</span> (513) 539-2673<br />
									<span class="blue">F:</span> (513) 539-4493<br />
									<span class="blue">Products:</span> Plastic tight head containers
								</p>
						  </div>
						</div>

						<a data-toggle="collapse" href="#newnanlist" aria-expanded="false" aria-controls="newnanlist" style="display: block"><span class="blue d-block">Newnan</span></a>
						<div class="collapse" id="newnanlist">
							<div class="card card-block" style="padding: .75rem">
								<p style="margin-bottom: 0;">
									98 Amlajack Blvd.<br />
									Newnan, Georgia 30265<br />
									<span class="blue">P:</span> (678) 423-2801<br />
									<span class="blue">F:</span> (678) 423-2947<br />
									<span class="blue">Products:</span> Plastic open head pails, screw top injection pails and drums
								</p>
						  </div>
						</div>

						<a data-toggle="collapse" href="#valparaisolist" aria-expanded="false" aria-controls="valparaisolist" style="display: block"><span class="blue d-block">Valparaiso</span></a>
						<div class="collapse" id="valparaisolist">
							<div class="card card-block" style="padding: .75rem">
								<p style="margin-bottom: 0;">
									4002 Montdale Drive<br />
									Valparaiso, Indiana 46383<br />
									<span class="blue">P:</span> (219) 462-8915<br />
									<span class="blue">F:</span> (219) 462-9641<br />
									<span class="blue">Products:</span> Plastic open head pails
								</p>
						  </div>
						</div>

					</div>
					<div class="col-12 col-sm-4 text-center">
						<span class="Canada d-block">Canada</span>

						<a data-toggle="collapse" href="#langleybclist" aria-expanded="false" aria-controls="langleybclist" style="display: block"><span class="green d-block">Langley BC</span></a>
						<div class="collapse" id="langleybclist">
							<div class="card card-block" style="padding: .75rem">
								<p style="margin-bottom: 0;">
									5850-272nd Street<br />
									Langley, British Columbia, Canada V4W 3Z1<br />
									<span class="green">P:</span> (604) 857-1177<br />
									<span class="green">F:</span> (604) 857-7747<br />
									<span class="green">Products:</span> Plastic open head pails, dairy containers, fish/misc products
								</p>
						  </div>
						</div>

						<a data-toggle="collapse" href="#oakvilleonlist" aria-expanded="false" aria-controls="oakvilleonlist" style="display: block"><span class="green d-block">Oakville ON</span></a>
						<div class="collapse" id="oakvilleonlist">
							<div class="card card-block" style="padding: .75rem">
								<p style="margin-bottom: 0;">
									2240 Wyecroft Road<br />
									Oakville, Ontario, Canada L6L 6M1<br />
									<span class="green">P:</span> (905) 827-9340<br />
									<span class="green">F:</span> (905) 827-8841<br />
									<span class="green">Products:</span> Plastic open head pails, dairy containers
								</p>
						  </div>
						</div>

						<a data-toggle="collapse" href="#springhillnslist" aria-expanded="false" aria-controls="springhillnslist" style="display: block"><span class="green d-block">Springhill NS</span></a>
						<div class="collapse" id="springhillnslist">
							<div class="card card-block" style="padding: .75rem">
								<p style="margin-bottom: 0;">
									29 Memorial Crescent<br />
									Springhill, Nova Scotia, Canada B0M 1X0<br />
									<span class="green">P:</span> (902) 597-3787<br />
									<span class="green">F:</span> (902) 597-8318<br />
									<span class="green">Products:</span> Plastic open head pails, dairy containers, fish/misc products
								</p>
						  </div>
						</div>

					</div>
					<div class="col-12 col-sm-4 text-center">
						<span class="Carribean d-block">Carribean</span>

						<a data-toggle="collapse" href="#cidraprlist" aria-expanded="false" aria-controls="cidraprlist" style="display: block"><span class="teal d-block">Cidra PR</span></a>
						<div class="collapse" id="cidraprlist">
							<div class="card card-block" style="padding: .75rem">
								<p style="margin-bottom: 0;">
									Road 172 KM 13.4<br />
									Cidra, Puerto Rico 00739<br />
									<span class="teal">P:</span> (787) 739-7401<br />
									<span class="teal">F:</span> (787) 739-7403<br />
									<span class="teal">Products:</span> Plastic drums, plastic tight head containers
								</p>
							</div>
						</div>

					</div>
					<div class="mx-auto mt-5" style="width: 80%"><p style="text-align: center">See additional facilities and capabilities at <a href="http://www.mausergroup.com" title="Mauser Group" target="_blank">mausergroup.com</a></p></div>
				</div>
			</div>
		</div>
	</div>
</div>

		<div class="row gray-back hidden-lg-down">
			<div class="col-md-12" style="margin-top: 85px;">
				<div class="map-nav">
					<center><h1>Everywhere You Are</h1>
					<p>In addition to its <strong>26 manufacturing facilities,</strong> BWAY partners with numerous distributors across the U.S., Canada, Mexico and the Caribbean. No matter where you are, we're never far.</p></center>
					<div class="nav-countries">
						<span class="US">United States</span>&nbsp; |
						<span class="Canada">Canada</span>&nbsp; |
						<span class="Carribean">Carribean</span>
					</div>
				</div>
				<div id="map-us">
						<a href="#" id="Executive" title="Executive Office" rel="executive" class="link"></a>
						<a href="#" id="Corporate" title="Corporate Office" rel="corporate" class="link"></a>
						<a href="#" id="Chicago" title="Chicago" rel="chicago" class="link"></a>
						<a href="#" id="Cincinnati" title="Cincinnati" rel="cincinnati" class="link"></a>
						<a href="#" id="Fontana" title="Fontana" rel="fontana" class="link"></a>
						<a href="#" id="Garland" title="Garland" rel="garland" class="link"></a>
						<a href="#" id="Homerville" title="Homerville" rel="homerville" class="link"></a>
						<a href="#" id="Memphis" title="Memphis" rel="memphis" class="link"></a>
						<a href="#" id="Sturtevant" title="Sturtevant" rel="sturtevant" class="link"></a>
						<a href="#" id="Trenton" title="Trenton" rel="trenton" class="link"></a>
						<a href="#" id="York" title="York" rel="york" class="link"></a>
						<a href="#" id="Bryan" title="Bryan" rel="bryan" class="link"></a>
						<a href="#" id="CedarCity" title="CedarCity" rel="cedarcity" class="link"></a>
						<a href="#" id="Dayton" title="Dayton" rel="dayton" class="link"></a>
						<a href="#" id="ElkGrove1" title="ElkGrove1" rel="elkGrove1" class="link"></a>
						<a href="#" id="ElkGrove2" title="ElkGrove2" rel="elkGrove2" class="link"></a>
						<a href="#" id="Indianapolis" title="Indianapolis" rel="indianapolis" class="link"></a>
						<a href="#" id="LaGrange" title="LaGrange" rel="lagrange" class="link"></a>
						<a href="#" id="Leominster" title="Leominster" rel="leominster" class="link"></a>
						<a href="#" id="Mansfield" title="Mansfield" rel="mansfield" class="link"></a>
						<a href="#" id="Monroe" title="Monroe" rel="monroe" class="link"></a>
						<a href="#" id="Newnan" title="Newnan" rel="newnan" class="link"></a>
						<a href="#" id="Valparaiso" title="Valparaiso" rel="valparaiso" class="link"></a>
						<a href="#" id="CidraPR" title="Cidra PR" rel="cidra" class="link"></a>
						<a href="#" id="Langley" title="Langley BC" rel="langley" class="link"></a>
						<a href="#" id="Oakville" title="Oakville ON" rel="oakville" class="link"></a>
						<a href="#" id="Springhill" title="Springhill NS" rel="springhill" class="link"></a>
				</div>

				<div id="map-info-box">
					<!-- Map Card Details -->
					<div class="info-box active" id="executive">
						<h3>Executive Office</h3>
						<p>1515 W. 22nd St.<br />
							Suite 1100<br />
							Oak Brook, Illinois 60523<br />
							<span class="blue">P:</span> (630) 203-4100<br />
							<span class="blue">F:</span> (630) 203-4110
						</p>
					</div>
					<!-- Map Card Details -->
					<div class="info-box active" id="corporate">
						<h3>Corporate Office</h3>
						<p>8607 Roberts Drive <br />
							Suite 250<br />
							Atlanta, Georgia 30350<br />
							<span class="blue">P:</span> (770) 645-4800<br />
							<span class="blue">F:</span> (770) 645-4810
						</p>
					</div>
					<!-- Map Card Details -->
					<div class="info-box active" id="chicago">
						<h3>Chicago, Illinois</h3>
						<p>3200 S. Kilbourn Ave.<br />
							Chicago, Illinois 60623<br />
							<span class="blue">P:</span> (773) 890-3300<br />
							<span class="blue">F:</span> (773) 254-4759
						</p>
						<p><span class="blue">Products:</span> Hybrid paint cans, metal food cans, metal paint cans, steel open head pails, steel tight head pails</p>
					</div>
					<!-- Map Card Details -->
					<div class="info-box" id="cincinnati">
						<h3>Cincinnati, Ohio</h3>
						<p>8200 Broadwell Road<br />
							Cincinnati, Ohio 45244<br />
							<span class="blue">P:</span> (513) 388-2200<br />
							<span class="blue">F:</span> (513) 388-2215
						</p>
						<p><span class="blue">Products:</span> Aerosol cans</p>
					</div>
					<!-- Map Card Details -->
					<div class="info-box" id="fontana">
						<h3>Fontana, California</h3>
						<p>11440 Pacific Ave.<br />
							Fontana, California 92337<br />
							<span class="blue">P:</span> (951) 361-4100<br />
							<span class="blue">F:</span> (951) 361-4110
						</p>
						<p><span class="blue">Products:</span> metal paint cans</p>
					</div>
					<!-- Map Card Details -->
					<div class="info-box" id="garland">
						<h3>Garland, Texas</h3>
						<p>3737 Miller Park Drive<br />
							Garland, Texas 75042<br />
							<span class="blue">P:</span> (972) 535-1100<br />
							<span class="blue">F:</span> (972) 535-1100
						</p>
						<p><span class="blue">Products:</span> F-style containers, metal paint cans</p>
					</div>
					<!-- Map Card Details -->
					<div class="info-box" id="homerville">
						<h3>Homerville, Georgia</h3>
						<p>1601 Valdosta Highway<br />
							Homerville, Georgia 31634<br />
							<span class="blue">P:</span> (912) 487-4000<br />
							<span class="blue">F:</span> (912) 487-4010
						</p>
						<p><span class="blue">Products:</span> Ammunition boxes, cone and pour top cans, metal paint cans, monotop cans, steel open head pails, steel tight head pails, oil cans</p>
					</div>
					<!-- Map Card Details -->
					<div class="info-box" id="memphis">
						<h3>Memphis, Tennessee</h3>
						<p>4651 Hickory Hill Road<br />
							Suite 104<br />
							Memphis, Tennessee 38141<br />
							<span class="blue">P:</span> (901) 251-2800<br />
							<span class="blue">F:</span> (901) 251-2810
						</p>
						<p><span class="blue">Products:</span> F-style containers</p>
					</div>
					<!-- Map Card Details -->
					<div class="info-box" id="sturtevant">
						<h3>Sturtevant, Wisconsin</h3>
						<p>10277 Venice Drive<br />
							Sturtevant, Wisconsin 53177<br />
							<span class="blue">P:</span> (262) 417-1201<br />
							<span class="blue">F:</span> (262) 417-1210
						</p>
						<p><span class="blue">Products:</span> Aerosol cans</p>
					</div>
					<!-- Map Card Details -->
					<div class="info-box" id="trenton">
						<h3>Trenton, New Jersey</h3>
						<p>Six Litho Road<br />
							Trenton, New Jersey 08638<br />
							<span class="blue">P:</span> (732) 997-4050<br />
							<span class="blue">F:</span> (732) 997-4055
						</p>
						<p><span class="blue">Products:</span> steel open head pails, steel tight head pails</p>
					</div>
					<!-- Map Card Details -->
					<div class="info-box" id="york">
						<h3>York, Pennsylvania</h3>
						<p>599 Davies Drive<br />
							York, Pennsylvania 17402<br />
							<span class="blue">P:</span> (717) 840-2100<br />
							<span class="blue">F:</span> (717) 840-2110
						</p>
						<p><span class="blue">Products:</span> Hybrid paint cans, metal paint cans, oil cans</p>
					</div>
					<!-- Map Card Details -->
					<div class="info-box" id="bryan">
						<h3>Bryan, Texas</h3>
						<p>1591 N. Harvey Mitchell Parkway<br />
							Bryan, Texas 77803<br />
							<span class="blue">P:</span> (979) 779-5900<br />
							<span class="blue">F:</span> (979) 823-1358
						</p>
						<p><span class="blue">Products:</span> Plastic open head pails, plastic tight head containers</p>
					</div>
					<!-- Map Card Details -->
					<div class="info-box" id="cedarcity">
						<h3>Cedar City, Utah</h3>
						<p>1033 N. Production Road<br />
							Cedar City, Utah 84721<br />
							<span class="blue">P:</span> (435) 865-1992<br />
							<span class="blue">F:</span> (435) 865-1993
						</p>
						<p><span class="blue">Products:</span> Plastic open head pails, plastic tight head containers, twist and lock pails</p>
					</div>
					<!-- Map Card Details -->
					<div class="info-box" id="dayton">
						<h3>Dayton, New Jersey</h3>
						<p>7 Wheeling Road<br />
							Dayton, New Jersey 08810<br />
							<span class="blue">P:</span> (732) 997-4100<br />
							<span class="blue">F:</span> (609) 860-0754
						</p>
						<p><span class="blue">Products:</span> Plastic open head pails, plastic tight head containers</p>
					</div>
					<!-- Map Card Details -->
					<div class="info-box" id="elkGrove1">
						<h3>Elk Grove Village, Illinois</h3>
						<p>1350 Arthur Ave.<br />
							Elk Grove Village, Illinois 60007<br />
							<span class="blue">P:</span> (847) 956-0750<br />
							<span class="blue">F:</span> (847) 956-0756
						</p>
						<p><span class="blue">Products:</span> Plastic open head pails</p>
					</div>
					<!-- Map Card Details -->
					<div class="info-box" id="elkGrove2">
						<h3>Elk Grove Village, Illinois</h3>
						<p>2350 Lively Blvd.<br />
							Elk Grove Village, Illinois 60007<br />
							<span class="blue">P:</span> (847) 860-4301<br />
							<span class="blue">F:</span> (847) 860-8197
						</p>
						<p><span class="blue">Products:</span> Plastic open head pails</p>
					</div>
					<!-- Map Card Details -->
					<div class="info-box" id="indianapolis">
						<h3>Indianapolis, Indiana</h3>
						<p>6061 Guion Road<br />
							Indianapolis, Indiana 46254<br />
							<span class="blue">P:</span> (317) 298-6155<br />
							<span class="blue">F:</span> (317) 298-6153
						</p>
						<p><span class="blue">Products:</span> Plastic tight head containers, plastic drums</p>
					</div>
					<!-- Map Card Details -->
					<div class="info-box" id="lagrange">
						<h3>LaGrange, Georgia</h3>
						<p>1603 Orchard Hill Road<br />
							LaGrange, Georgia 30240<br />
							<span class="blue">P:</span> (706) 885-1772<br />
							<span class="blue">F:</span> (706) 812-9586
						</p>
						<p><span class="blue">Products:</span> Plastic open head pails, plastic tight head containers</p>
					</div>
					<!-- Map Card Details -->
					<div class="info-box" id="leominster">
						<h3>Leominster, Massachusetts</h3>
						<p>196 Industrial Road<br />
							Leominster, Massachusetts 01453<br />
							<span class="blue">P:</span> (978) 537-4911<br />
							<span class="blue">F:</span> (978) 537-6376
						</p>
						<p><span class="blue">Products:</span> Plastic open head pails, twist and lock pails</p>
					</div>
					<!-- Map Card Details -->
					<div class="info-box" id="mansfield">
						<h3>Mansfield, Texas</h3>
						<p>1501 E. Dallas St.<br />
							Mansfield, Texas 76063<br />
							<span class="blue">P:</span> (817) 473-0259<br />
							<span class="blue">F:</span> (817) 473-8185
						</p>
						<p><span class="blue">Products:</span> Plastic open head pails, plastic tight head containers</p>
					</div>
					<!-- Map Card Details -->
					<div class="info-box" id="monroe">
						<h3>Monroe, Ohio</h3>
						<p>980 Deneen Ave.<br />
							Monroe, Ohio 45050<br />
							<span class="blue">P:</span> (513) 539-2673<br />
							<span class="blue">F:</span> (513) 539-4493
						</p>
						<p><span class="blue">Products:</span> Plastic tight head containers</p>
					</div>
					<!-- Map Card Details -->
					<div class="info-box" id="newnan">
						<h3>Newnan, Georgia</h3>
						<p>98 Amlajack Blvd.<br />
							Newnan, Georgia 30265<br />
							<span class="blue">P:</span> (678) 423-2801<br />
							<span class="blue">F:</span> (678) 423-2947
						</p>
						<p><span class="blue">Products:</span> Plastic open head pails, screw top injection pails and drums</p>
					</div>
					<!-- Map Card Details -->
					<div class="info-box" id="valparaiso">
						<h3>Valparaiso, Indiana</h3>
						<p>4002 Montdale Drive<br />
							Valparaiso, Indiana 46383<br />
							<span class="blue">P:</span> (219) 462-8915<br />
							<span class="blue">F:</span> (219) 462-9641
						</p>
						<p><span class="blue">Products:</span> Plastic open head pails</p>
					</div>
					<!-- Map Card Details -->
					<div class="info-box" id="cidra" style="border-bottom: 20px solid #00b1ac">
						<h3 style="color: #00b1ac">Cidra, Puerto Rico</h3>
						<p>Road 172 KM 13.4<br />
							Cidra, Puerto Rico 00739<br />
							<span class="teal">P:</span> (787) 739-7401<br />
							<span class="teal">F:</span> (787) 739-7403
						</p>
						<p><span class="teal">Products:</span> Plastic drums, plastic tight head containers</p>
					</div>
					<!-- Map Card Details -->
					<div class="info-box" id="langley" style="border-bottom: 20px solid #5a8e21">
						<h3 style="color: #5a8e21">Langley, British Columbia, Canada</h3>
						<p>5850-272nd Street<br />
							Langley, British Columbia, Canada V4W 3Z1<br />
							<span class="green">P:</span> (604) 857-1177<br />
							<span class="green">F:</span> (604) 857-7747
						</p>
						<p><span class="green">Products:</span> Plastic open head pails, dairy containers, fish/misc products</p>
					</div>
					<!-- Map Card Details -->
					<div class="info-box" id="oakville" style="border-bottom: 20px solid #5a8e21">
						<h3 style="color: #5a8e21">Oakville, Ontario, Canada</h3>
						<p>2240 Wyecroft Road<br />
							Oakville, Ontario, Canada L6L 6M1<br />
							<span class="green">P:</span> (905) 827-9340<br />
							<span class="green">F:</span> (905) 827-8841
						</p>
						<p><span class="green">Products:</span> Plastic open head pails, dairy containers</p>
					</div>
					<!-- Map Card Details -->
					<div class="info-box" id="springhill" style="border-bottom: 20px solid #5a8e21">
						<h3 style="color: #5a8e21">Springhill, Nova Scotia, Canada</h3>
						<p>29 Memorial Crescent<br />
							Springhill, Nova Scotia, Canada B0M 1X0<br />
							<span class="green">P:</span> (902) 597-3787<br />
							<span class="green">F:</span> (902) 597-8318
						</p>
						<p><span class="green">Products:</span> Plastic open head pails, dairy containers, fish/misc products</p>
					</div>
					<center><p>See additional facilities and capabilities at <a href="http://www.mausergroup.com" title="Mauser Group" target="_blank">mausergroup.com</a></p></center>
				</div>
		</div>
	</div>

		<div class="row careers align-items-center justify-content-center mt-5">
			<div class="col-md-5 align-self-center">
				<div class="col-md-8 offset-md-2">
					<center><h1>Join Our Team</h1>
					<p>Do you have a desire to change the world around you?<br />BWAY is always on the lookout for inspired professionals.<br /><br />
					<a href="http://www.jobs.net/jobs/bway/en-us/" target="_blank" class="btn" style="background: transparent; color:#fff;font-weight:normal;">SEE OPEN POSITIONS</a></p></center>
				</div>
			</div>
		</div>

		<div class="row pioneers align-items-center">
			<div class="col-12 col-sm-12 col-md-5 offset-md-2">
				<?php echo do_shortcode('[KGVID poster="https://bwaycorp.com/wp-content/uploads/2017/09/BWAY_137_Man_Marvels_Video_640x360.png"]https://bwaycorp.com/wp-content/uploads/Bway Manufacturing Marvels.mp4[/KGVID]') ?>
			</div>
			<div class="col-md-4">
				<h2>PIONEERS - Not Just Manufacturers</h2>
				<p>Innovation is a cornerstone of our culture. We partner with customers to envision, design, develop and refine the packaging solutions of tomorrow.</p>
			</div>
			<div class="col-12 col-md-4 offset-md-1">
				<center><img src="<?php echo $upload_dir['baseurl']; ?>/2017/06/pulse-icon.png" />
				<p style="margin-top: 10px; color: #464646"><strong>BWAY PULSE</strong></p>
				<p style="color: #464646">Get the latest BWAY news, insights and updates, all in one place.<br /><br /><br /><br />
				<a href="<?php echo site_url(); ?>/about/news-and-events" class="btn">STAY IN THE KNOW</a></p></center>
			</div>
			<div class="col-12 col-md-4 offset-md-1 offset-lg-2">
				<center><img src="<?php echo $upload_dir['baseurl']; ?>/2017/06/form-icon.png" />
				<p style="margin-top: 10px; color: #464646"><strong>WE'RE HERE TO HELP</strong></p>
				<p style="color: #464646">Have questions about BWAY's products, markets or distributors? Enter your information below and we'll put you in touch with your own personal BWAY consultant.<br />
				<a href="<?php echo site_url(); ?>/contact" class="btn">GET HELP</a></p></center>
			</div>
		</div>
	</div><!-- .entry-content -->

</article><!-- #post-## -->

<script>
		jQuery(window).load(function() {
	  jQuery('#carousel').flexslider({
	    animation: "slide",
	    controlNav: false,
	    animationLoop: false,
	    slideshow: false,
	    itemWidth: 145,
	    itemMargin: 15,
			maxItems: 8,
			move: 1,
	    asNavFor: '#slider'
	  });

	  jQuery('#slider').flexslider({
	    animation: "slide",
			animationSpeed: 400,
	    controlNav: false,
	    animationLoop: false,
	    slideshow: false,
			touch: true,
	    sync: "#carousel"
		  });
		});

		jQuery(document).ready(function () {
			// Catch all clicks on a link with the class 'link'
				jQuery('.link').click(function(e) {
			    // Stop the link being followed:
			    e.preventDefault();
			    // Get the div to be shown:
			    var content = jQuery(this).attr('rel');
			    // Remove any active classes:
			    jQuery('.active').removeClass('active');
			    // Add the 'active' class to this link:
			   	jQuery('[rel="' + content + '"]').addClass('active');
			    // Hide all the content:
			    jQuery('.info-box').hide();
			    // Show the requested content:
 					jQuery('#' + content).fadeIn(800);
				});

			});

</script>
