<?php
/**
 * Template for End Markets pages.
 *
 * @package understrap
 */

$upload_dir = wp_upload_dir();



?>

	<div class="container-fluid">
		<div class="row justify-content-center headers" style="background: url('<?php the_field('header_image'); ?>') no-repeat center center;">
		</div>
	</div>

	<div class="entry-content">

		<div class="container" style="margin-bottom: 100px;">
				<?php the_content(); ?>
				<?php if( get_field('product-request') ): ?>
					<br />
					<img class="aligncenter size-full wp-image-303 mr-3" src="<?php echo site_url(); ?>/wp-content/uploads/2017/07/blue-spec-icon.png" alt="" width="19" height="23" />

					<?php if( have_rows('product_catalog_link_options') ): while( have_rows('product_catalog_link_options') ): the_row();

							$general_link = get_sub_field('general_catalog_link');
							$specific_link = get_sub_field('product_catalog_link');
					?>

					<?php	if( $general_link == true ){ ?>
					        <a href="<?php echo site_url(); ?>/products/product-intro" style="color: #004b8d; font-weight: bold;">PRODUCT CATALOG</a>
					<?php } else { ?>
						<?php $term_id = $specific_link;
								if( $term_id):
										$term_name = get_cat_name( $term_id ) ;
										$term_url = get_category_link( $term_id ); ?>
										<a href="<?php echo $term_url ?>" style="color: #004b8d; font-weight: bold;">PRODUCT CATALOG</a>
							 <?php endif; ?>
					<?php } ?>

						<?php endwhile; ?>

					<?php endif; ?>

					<br /><br />
					<img class="aligncenter size-full wp-image-304 mr-3" src="<?php echo site_url(); ?>/wp-content/uploads/2017/07/info-icon.png" alt="" width="20" height="23" /><a style="color: #004b8d; font-weight: bold;" href="<?php echo site_url(); ?>/contact">REQUEST MORE INFORMATION</a>
				<?php endif; ?>
		</div>

				<?php if( have_rows('product-rows') ): ?>
					<?php while( have_rows('product-rows') ): the_row();

						// vars
						$product_image = get_sub_field('product-image');
						$image_link = get_sub_field('image_link_to_category');
						$link_color = get_sub_field('link-color');
						$title = get_sub_field('title');
						$description = get_sub_field('description');
						$spec_sheet = get_sub_field('spec-sheet-title');
						$spec_sheet_link = get_sub_field('spec-sheet-link');
						$spec_sheet_2 = get_sub_field('spec-sheet-title_2');
						$spec_sheet_link_2 = get_sub_field('spec-sheet-link_2');
						?>

		<div class="container-fluid">
			<div class="row auto-angle-back">
				<div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-3 offset-sm-3 offset-md-0 offset-lg-1 offset-xl-2 pt-5">

					<?php if( $image_link == '' ): ?>
						<img src="<?php echo $product_image; ?>" alt="<?php echo $title; ?>" class="img-fluid d-flex align-self-center max-thumb"/>
					<?php else: ?>
					<?php $term_id = $image_link;
							if( $term_id):
									$term_name = get_cat_name( $term_id ) ;
									$term_url = get_category_link( $term_id ); ?>
									<a href="<?php echo $term_url ?>"><img src="<?php echo $product_image; ?>" alt="<?php echo $title; ?>" class="img-fluid d-flex align-self-center max-thumb"/></a>
						 <?php endif; ?>
					<?php endif; ?>
				</div>
				<div class="col-12 col-sm-10 col-md-8 col-lg-6 col-xl-4 offset-sm-1 offset-md-0 offset-lg-2 offset-xl-2 pt-5 pb-4 flex-column">
					<h2 class="<?php echo $link_color; ?>" style="text-transform: uppercase"><?php echo $title; ?></h2>
					<p><?php echo $description; ?></p><br />
					<?php if( get_sub_field('link-color') == 'blue' ): ?><img src="<?php echo $upload_dir['baseurl']; ?>/2017/07/blue-spec-icon.png" class="mr-2" /><?php else: ?><img src="<?php echo $upload_dir['baseurl']; ?>/2017/07/green-spec-icon.png" class="mr-2" /><?php endif; ?><a href="<?php echo $spec_sheet_link; ?>" title="<?php echo $spec_sheet_2; ?>" alt="<?php echo $spec_sheet; ?>" class="<?php echo $link_color; ?>" style="font-size: .8rem; text-transform: uppercase" target="_blank"><?php echo $spec_sheet; ?></a>
						<?php if ( get_sub_field( 'spec-sheet-title_2' ) ): ?><br /><br />
					<?php if( get_sub_field('link-color') == 'blue' ): ?><img src="<?php echo $upload_dir['baseurl']; ?>/2017/07/blue-spec-icon.png" class="mr-2" /><?php else: ?><img src="<?php echo $upload_dir['baseurl']; ?>/2017/07/green-spec-icon.png" class="mr-2" /><?php endif; ?><a href="<?php echo $spec_sheet_link_2; ?>" title="<?php echo $spec_sheet_2; ?>" alt="<?php echo $spec_sheet_2; ?>" class="<?php echo $link_color; ?>" style="font-size: .8rem; text-transform: uppercase" target="_blank"><?php echo $spec_sheet_2; ?></a>
						<?php endif; ?>
				</div>
			</div>
		</div>

			<?php endwhile; ?>
		<?php endif; ?>

<?php if( get_field('call_email') ): ?>
		<div class="container-fluid" style="border-top: 2px solid #a7a9ac;">
			<div class="row justify-content-center" style="margin: 100px 0;">
				<div class="col-sm-12 col-md-8 col-lg-6">
					<h2 style="color: #58595b; text-align: center; font-family: DIN-Medium; font-weight: bold; text-transform:uppercase">Have Questions<br />About Our Products?</h2>
					<div class="container">
						<div class="row justify-content-center mt-5">
							<div class="col-sm-3 col-md-3">
								<center><a href="tel:8005272267" title="Call Us at 800.527.2267"><img src="<?php echo $upload_dir['baseurl']; ?>/2017/07/phone-icon.png" /></center><br />
								<p style="text-align: center; font-weight: bold;">800.527.2267</a></p>
							</div>
							<div class="col-sm-3 col-md-3">
								<center><a href="mailto:sales@bwaycorp.com?subject=Product%20Question" title="Contact Us"><img src="<?php echo $upload_dir['baseurl']; ?>/2017/07/email-icon.png" /></center><br />
								<p style="text-align: center; font-weight: bold;">EMAIL</a></p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
<?php endif; ?>

	</div><!-- .entry-content -->

</article><!-- #post-## -->
