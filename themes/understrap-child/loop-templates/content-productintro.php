<?php
/**
 * Partial template for content in product-intro.php
 *
 * @package understrap
 */
$upload_dir = wp_upload_dir();
?>
<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">

		<div class="container-fluid">
			<div class="row pg-headers" style="background: url('<?php echo get_stylesheet_directory_uri(); ?>/images/gray-product-intro-back-temp.png') no-repeat center center;">
				<div class="container">
					<div class="row mt-4 pt-4">
						<div class="col-10 col-md-5 offset-md-1 mt-lg-5 pt-lg-5">
							<h2 class="align-middle" style="color:#004b8d">FEATURED</h2>
							<p>Browse our product catalog to see just how far and wide our products span.</p>
							<p><a href="<?php echo site_url(); ?>/product-category/plastic-products/plastic-open-head-pails/" class="btn-blue" style="margin-top: 10px;">Plastic Open Head Pails</a></p>
						</div>
						<div class="col-10 col-md-5">
							<img src="<?php echo site_url(); ?>/wp-content/uploads/2017/08/featured-Plastic_1Ltr_OpenHead_Products.png" />
						</div>
					</div>
				</div>
			</div>
		</div>

<div class="entry-content">

	<div class="container">
		<div class="row justify-content-center" style="margin: 40px 0 40px 0;">
			<?php the_title( '<h1 class="entry-title" style="color: #004b8d; font-weight: bold; text-transform: uppercase;">', '</h1>' ); ?>
		</div>
	</div>

	<div class="container mb-5">
		<?php the_content(); ?>
	</div>

	<div class="container-fluid">
		<div class="row justify-content-center pt-5 mb-5">
			<div class="col-md-4 col-lg-3 col-xl-2" style="padding: 3rem; border: 2px solid #929497; border-bottom: 40px solid #004a8d">
				<p class="text-center mb-4 product-intro-title plastic">Plastic</p>
				<ul class="prod-cat-list plastic">
			    <?php wp_list_categories( array(
			        'orderby'            => 'name',
			        'show_count'         => false,
			        'use_desc_for_title' => false,
							'taxonomy' 					 => 'product_cat',
							'title_li'            => __( '' ),
							'style'               => 'list',
			        'child_of'           => 686
			    ) ); ?>
				</ul>
			</div>
			<div class="col-md-4 col-lg-3 col-xl-2 offset-md-1" style="padding: 3rem; border: 2px solid #929497; border-bottom: 40px solid #638b18">
				<p class="text-center mb-4 product-intro-title metal">Metal</p>
				<ul class="prod-cat-list metal">
					<?php wp_list_categories( array(
							'orderby'            => 'name',
							'show_count'         => false,
							'use_desc_for_title' => false,
							'taxonomy' 					 => 'product_cat',
							'title_li'            => __( '' ),
							'style'               => 'list',
							'child_of'           => 636
					) ); ?>
				</ul>
			</div>
		</div>
	</div>

	<div class="container-fluid" style="border-top: 2px solid #a7a9ac;">
		<div class="row justify-content-center" style="margin: 100px 0;">
			<div class="col-sm-12 col-md-8 col-lg-6">
				<h4 style="color: #58595b; text-align: center; font-family: DIN-Medium; font-weight: bold; text-transform:uppercase">Have Questions<br />About Our Products?</h4>
					<div class="container">
						<div class="row justify-content-center mt-5">
							<div class="col-sm-3 col-md-3">
								<center><a href="tel:8005272267" title="Call Us at 800.527.2267"><img src="<?php echo $upload_dir['baseurl']; ?>/2017/07/phone-icon.png" /></center><br />
								<p style="text-align: center; font-weight: bold;">800.527.2267</a></p>
							</div>
							<div class="col-sm-3 col-md-3">
								<center><a href="mailto:sales@bwaycorp.com?subject=Product%20Question" title="Contact Us"><img src="<?php echo $upload_dir['baseurl']; ?>/2017/07/email-icon.png" /></center><br />
								<p style="text-align: center; font-weight: bold;">EMAIL</a></p>
							</div>
						</div>
					</div>
			</div>
		</div>
	</div>

</div><!-- .entry-content -->

</article><!-- #post-## -->
